<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* load the MX_Router class */
require APPPATH . "third_party/MX/Controller.php";

class MY_Controller extends MX_Controller
{	
    protected $data;
	function __construct() 
	{
		parent::__construct();
		$this->_hmvc_fixes();
	}
	
	function _hmvc_fixes()
	{		
		//fix callback form_validation		
		//https://bitbucket.org/wiredesignz/codeigniter-modular-extensions-hmvc
		$this->load->library('form_validation');
		$this->form_validation->CI =& $this;
	}
	
	/**
	 * Displays the specified view
	 * @param array $data
	 */
	function _render_page($view, $data=null, $returnhtml=false)
	{
	    $this->viewdata = (empty($data)) ? $this->data: $data;
	    $view_html = $this->load->view($view, $this->viewdata, $returnhtml);
	    if ($returnhtml) return $view_html;//This will return html on 3rd argument being true
	}

}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */
