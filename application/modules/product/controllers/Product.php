<?php
class Product extends MY_Controller
{
    
    private $template;
    public function __construct()
    {
        parent::__construct();
        $this->template = "template/index";
    }
    
    public function all_products(){
        $this->data['content'] = "all";
        $this->data['title'] = 'All Products';
        $this->_render_page($this->template, $this->data);
    }
    
}

