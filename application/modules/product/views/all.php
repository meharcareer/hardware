<div class="wrapper">
	<div class="grid__item">
		<div class="grid__item">
			<div class="collection-products">
				<div class="grid__item wide--four-fifths post-large--four-fifths large--seven-tenths sidebar-hidden">
					<div class="collection-list">
						<div class="grid-uniform grid-link__container col-main">
							<div class="products-grid-view">
								<ul>
									<li class="grid__item wow fadeInUp item-row  wide--one-quarter post-large--one-third large--one-half medium--one-half small-grid__item sold-out" id="product-742929203243">
										<div class="products product-">
											<div class="product-container">
												<a href="../products/product-10.html" class="grid-link"> 
													<div class="reveal">
														<span class="product-additional"> 
															<img src="<?php echo base_url('assets/');?>cdn.shopify.com/s/files/1/2721/6956/products/product25bbc.jpg?v=1518071378" class="featured-image" alt="9 meters inch tap">
														</span> 
														<img src="<?php echo base_url('assets/');?>cdn.shopify.com/s/files/1/2721/6956/products/product22_3819f9fe-06f5-40a8-b7cb-8da6d8d94f7a1bc5.jpg?v=1518075700" class="hidden-feature_img" alt="9 meters inch tap" />
													</div>
												</a>
											</div>
											<div class="product-detail">
												<a href="../products/product-10.html"
													class="grid-link__title">9 meters inch tap</a>
											</div>
										</div>
									</li>











									<li
										class="grid__item wow fadeInUp item-row  wide--one-quarter post-large--one-third large--one-half medium--one-half small-grid__item"
										id="product-742929498155">
										<div class="products product-">

											<div class="product-container">









												<a href="../products/product-12.html" class="grid-link">




													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url('assets/');?>cdn.shopify.com/s/files/1/2721/6956/products/product84f4e.jpg?v=1518071484"
															class="featured-image" alt="A pair of brushes">
														</span> <img
															src="<?php echo base_url('assets/');?>cdn.shopify.com/s/files/1/2721/6956/products/product90c7e.jpg?v=1518077447"
															class="hidden-feature_img" alt="A pair of brushes" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">



														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742929498155">
															<input type="hidden" name="id" value="10350475903019" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>







														<a href="javascript:void(0)" id="product-12"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>



														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-12 loading">
																	<a class="add-in-wishlist-js" href="product-12.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-12 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-12.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-12 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="../pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>



														<a href="#" class="compare action-home4"
															data-pid="product-12" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="../products/product-12.html"
													class="grid-link__title">A pair of brushes</a>


												<div class="grid-link__meta">
													<div class="product_price">


														<div class="grid-link__org_price">
															<span class=money>$300.00</span>
														</div>


													</div>


												</div>

											</div>


										</div>
									</li>











									<li
										class="grid__item wow fadeInUp item-row  wide--one-quarter post-large--one-third large--one-half medium--one-half small-grid__item"
										id="product-742930710571">
										<div class="products product-">

											<div class="product-container">









												<a href="../products/product-22.html" class="grid-link">




													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url('assets/');?>cdn.shopify.com/s/files/1/2721/6956/products/product30aef3.jpg?v=1518071982"
															class="featured-image" alt="Carpentry tools set">
														</span> <img
															src="<?php echo base_url('assets/');?>cdn.shopify.com/s/files/1/2721/6956/products/product2854ed.jpg?v=1518085299"
															class="hidden-feature_img" alt="Carpentry tools set" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">



														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930710571">
															<input type="hidden" name="id" value="8508816523307" /> <a
																class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>







														<a href="javascript:void(0)" id="product-22"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>



														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-22 loading">
																	<a class="add-in-wishlist-js" href="product-22.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-22 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-22.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-22 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="../pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>



														<a href="#" class="compare action-home4"
															data-pid="product-22" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="../products/product-22.html"
													class="grid-link__title">Carpentry tools set</a>


												<div class="grid-link__meta">
													<div class="product_price">


														<div class="grid-link__org_price">
															<span class=money>$753.00</span>
														</div>


													</div>


												</div>

											</div>


										</div>
									</li>











									<li
										class="grid__item wow fadeInUp item-row  wide--one-quarter post-large--one-third large--one-half medium--one-half small-grid__item"
										id="product-742930907179">
										<div class="products product-">

											<div class="product-container">









												<a href="../products/product-24.html" class="grid-link">




													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url('assets/');?>cdn.shopify.com/s/files/1/2721/6956/products/product32cf18.jpg?v=1518071999"
															class="featured-image" alt="Clamps &amp; Reducers">
														</span> <img
															src="<?php echo base_url('assets/');?>cdn.shopify.com/s/files/1/2721/6956/products/product3106a9.jpg?v=1518086562"
															class="hidden-feature_img" alt="Clamps &amp; Reducers" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">



														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930907179">
															<input type="hidden" name="id" value="8508817342507" /> <a
																class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>







														<a href="javascript:void(0)" id="product-24"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>



														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-24 loading">
																	<a class="add-in-wishlist-js" href="product-24.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-24 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-24.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-24 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="../pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>



														<a href="#" class="compare action-home4"
															data-pid="product-24" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="../products/product-24.html"
													class="grid-link__title">Clamps & Reducers</a>


												<div class="grid-link__meta">
													<div class="product_price">


														<div class="grid-link__org_price">
															<span class=money>$200.00</span>
														</div>


													</div>


												</div>

											</div>


										</div>
									</li>











									<li
										class="grid__item wow fadeInUp item-row  wide--one-quarter post-large--one-third large--one-half medium--one-half small-grid__item on-sale"
										id="product-742930645035">
										<div class="products product-">

											<div class="product-container">









												<a href="../products/product-21.html" class="grid-link">


													<div class="featured-tag">
														<span class="badge badge--sale"> <span
															class="gift-tag badge__text">Sale</span>
														</span>
													</div>



													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url('assets/');?>cdn.shopify.com/s/files/1/2721/6956/products/product484bd.jpg?v=1518071901"
															class="featured-image" alt="Cutting Pliers Set">
														</span> <img
															src="<?php echo base_url('assets/');?>cdn.shopify.com/s/files/1/2721/6956/products/product15_52e33829-bf2d-4ec4-a4c7-272564e64e091dda.jpg?v=1518084989"
															class="hidden-feature_img" alt="Cutting Pliers Set" />
													</div>

												</a>
												<div class="product_right_tag   offer_exist "></div>
												<div class="ImageWrapper">
													<div class="product-button">



														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930645035">
															<input type="hidden" name="id" value="8508816392235" /> <a
																class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>







														<a href="javascript:void(0)" id="product-21"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>



														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-21 loading">
																	<a class="add-in-wishlist-js" href="product-21.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-21 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-21.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-21 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="../pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>



														<a href="#" class="compare action-home4"
															data-pid="product-21" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="../products/product-21.html"
													class="grid-link__title">Cutting Pliers Set</a>


												<div class="grid-link__meta">
													<div class="product_price">


														<div class="grid-link__org_price">
															<span class=money>$300.00</span>
														</div>


														<del class="grid-link__sale_price">
															<span class=money>$400.00</span>
														</del>

													</div>


												</div>

											</div>


										</div>
									</li>











									<li
										class="grid__item wow fadeInUp item-row  wide--one-quarter post-large--one-third large--one-half medium--one-half small-grid__item on-sale"
										id="product-742930874411">
										<div class="products product-">

											<div class="product-container">









												<a href="../products/product-23.html" class="grid-link">


													<div class="featured-tag">
														<span class="badge badge--sale"> <span
															class="gift-tag badge__text">Sale</span>
														</span>
													</div>



													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url('assets/');?>cdn.shopify.com/s/files/1/2721/6956/products/product1107cf.jpg?v=1518071990"
															class="featured-image" alt="Cutting Pliers Set II">
														</span> <img
															src="<?php echo base_url('assets/');?>cdn.shopify.com/s/files/1/2721/6956/products/product15_cbbfd503-5aad-4d1f-b47f-8ff8c3af1cf7ca99.jpg?v=1518086366"
															class="hidden-feature_img" alt="Cutting Pliers Set II" />
													</div>

												</a>
												<div class="product_right_tag   offer_exist "></div>
												<div class="ImageWrapper">
													<div class="product-button">



														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930874411">
															<input type="hidden" name="id" value="8508816752683" /> <a
																class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>







														<a href="javascript:void(0)" id="product-23"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>



														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-23 loading">
																	<a class="add-in-wishlist-js" href="product-23.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-23 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-23.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-23 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="../pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>



														<a href="#" class="compare action-home4"
															data-pid="product-23" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="../products/product-23.html"
													class="grid-link__title">Cutting Pliers Set II</a>


												<div class="grid-link__meta">
													<div class="product_price">


														<div class="grid-link__org_price">
															<span class=money>$399.00</span>
														</div>


														<del class="grid-link__sale_price">
															<span class=money>$600.00</span>
														</del>

													</div>


												</div>

											</div>


										</div>
									</li>











									<li
										class="grid__item wow fadeInUp item-row  wide--one-quarter post-large--one-third large--one-half medium--one-half small-grid__item"
										id="product-742930087979">
										<div class="products product-">

											<div class="product-container">









												<a href="../products/product-16.html" class="grid-link">




													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url('assets/');?>cdn.shopify.com/s/files/1/2721/6956/products/product259157.jpg?v=1518071595"
															class="featured-image" alt="Drilling Machine">
														</span> <img
															src="<?php echo base_url('assets/');?>cdn.shopify.com/s/files/1/2721/6956/products/product22_1ef27c40-1fb7-4974-918c-d2f7bc23426e27b6.jpg?v=1518083068"
															class="hidden-feature_img" alt="Drilling Machine" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">



														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930087979">
															<input type="hidden" name="id" value="10354571051051" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>







														<a href="javascript:void(0)" id="product-16"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>



														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-16 loading">
																	<a class="add-in-wishlist-js" href="product-16.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-16 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-16.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-16 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="../pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>



														<a href="#" class="compare action-home4"
															data-pid="product-16" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="../products/product-16.html"
													class="grid-link__title">Drilling Machine</a>


												<div class="grid-link__meta">
													<div class="product_price">


														<div class="grid-link__org_price">
															<span class=money>$500.00</span>
														</div>


													</div>


												</div>

											</div>


										</div>
									</li>











									<li
										class="grid__item wow fadeInUp item-row  wide--one-quarter post-large--one-third large--one-half medium--one-half small-grid__item"
										id="product-742931202091">
										<div class="products product-">

											<div class="product-container">









												<a href="../products/product-27.html" class="grid-link">




													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url('assets/');?>cdn.shopify.com/s/files/1/2721/6956/products/product229245.jpg?v=1518072070"
															class="featured-image" alt="Hacksaw Blades Kit">
														</span> <img
															src="<?php echo base_url('assets/');?>cdn.shopify.com/s/files/1/2721/6956/products/product12_31bd55cf-6146-46ae-a11c-8fc2f076eddebce2.jpg?v=1518087563"
															class="hidden-feature_img" alt="Hacksaw Blades Kit" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">



														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742931202091">
															<input type="hidden" name="id" value="8508820422699" /> <a
																class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>







														<a href="javascript:void(0)" id="product-27"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>



														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-27 loading">
																	<a class="add-in-wishlist-js" href="product-27.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-27 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-27.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-27 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="../pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>



														<a href="#" class="compare action-home4"
															data-pid="product-27" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="../products/product-27.html"
													class="grid-link__title">Hacksaw Blades Kit</a>


												<div class="grid-link__meta">
													<div class="product_price">


														<div class="grid-link__org_price">
															<span class=money>$389.00</span>
														</div>


													</div>


												</div>

											</div>


										</div>
									</li>











									<li
										class="grid__item wow fadeInUp item-row  wide--one-quarter post-large--one-third large--one-half medium--one-half small-grid__item"
										id="product-742929334315">
										<div class="products product-">

											<div class="product-container">









												<a href="../products/product-11.html" class="grid-link">




													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url('assets/');?>cdn.shopify.com/s/files/1/2721/6956/products/product5e640.jpg?v=1518077110"
															class="featured-image" alt="Hammer">
														</span> <img
															src="<?php echo base_url('assets/');?>cdn.shopify.com/s/files/1/2721/6956/products/product21e640.jpg?v=1518077110"
															class="hidden-feature_img" alt="Hammer" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">



														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742929334315">
															<input type="hidden" name="id" value="10350283063339" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>







														<a href="javascript:void(0)" id="product-11"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>



														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-11 loading">
																	<a class="add-in-wishlist-js" href="product-11.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-11 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-11.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-11 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="../pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>



														<a href="#" class="compare action-home4"
															data-pid="product-11" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="../products/product-11.html"
													class="grid-link__title">Hammer</a>


												<div class="grid-link__meta">
													<div class="product_price">


														<div class="grid-link__org_price">
															<span class=money>$500.00</span>
														</div>


													</div>


												</div>

											</div>


										</div>
									</li>











									<li
										class="grid__item wow fadeInUp item-row  wide--one-quarter post-large--one-third large--one-half medium--one-half small-grid__item"
										id="product-742930972715">
										<div class="products product-">

											<div class="product-container">









												<a href="../products/product-25.html" class="grid-link">




													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url('assets/');?>cdn.shopify.com/s/files/1/2721/6956/products/product26ec91.jpg?v=1518086938"
															class="featured-image" alt="Hammer Set">
														</span> <img
															src="<?php echo base_url('assets/');?>cdn.shopify.com/s/files/1/2721/6956/products/product25_a0bc1aca-1422-482e-a99e-611a9d2cc1400523.jpg?v=1518086948"
															class="hidden-feature_img" alt="Hammer Set" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">



														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930972715">
															<input type="hidden" name="id" value="10357560999979" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>







														<a href="javascript:void(0)" id="product-25"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>



														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-25 loading">
																	<a class="add-in-wishlist-js" href="product-25.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-25 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-25.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-25 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="../pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>



														<a href="#" class="compare action-home4"
															data-pid="product-25" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="../products/product-25.html"
													class="grid-link__title">Hammer Set</a>


												<div class="grid-link__meta">
													<div class="product_price">


														<div class="grid-link__org_price">
															<span class=money>$423.00</span>
														</div>


													</div>


												</div>

											</div>


										</div>
									</li>











									<li
										class="grid__item wow fadeInUp item-row  wide--one-quarter post-large--one-third large--one-half medium--one-half small-grid__item on-sale"
										id="product-742929924139">
										<div class="products product-">

											<div class="product-container">









												<a href="../products/product-15.html" class="grid-link">


													<div class="featured-tag">
														<span class="badge badge--sale"> <span
															class="gift-tag badge__text">Sale</span>
														</span>
													</div>



													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url('assets/');?>cdn.shopify.com/s/files/1/2721/6956/products/product18f17f.jpg?v=1518071570"
															class="featured-image" alt="Hardware Starter kit">
														</span> <img
															src="<?php echo base_url('assets/');?>cdn.shopify.com/s/files/1/2721/6956/products/product30_efaa5cbd-99df-44b7-901e-b9970e7375132297.jpg?v=1518082211"
															class="hidden-feature_img" alt="Hardware Starter kit" />
													</div>

												</a>
												<div class="product_right_tag   offer_exist "></div>
												<div class="ImageWrapper">
													<div class="product-button">



														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742929924139">
															<input type="hidden" name="id" value="10354033885227" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>







														<a href="javascript:void(0)" id="product-15"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>



														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-15 loading">
																	<a class="add-in-wishlist-js" href="product-15.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-15 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-15.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-15 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="../pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>



														<a href="#" class="compare action-home4"
															data-pid="product-15" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="../products/product-15.html"
													class="grid-link__title">Hardware Starter kit</a>


												<div class="grid-link__meta">
													<div class="product_price">


														<div class="grid-link__org_price">
															<span class=money>$598.00</span>
														</div>


														<del class="grid-link__sale_price">
															<span class=money>$789.00</span>
														</del>

													</div>


												</div>

											</div>


										</div>
									</li>











									<li
										class="grid__item wow fadeInUp item-row  wide--one-quarter post-large--one-third large--one-half medium--one-half small-grid__item"
										id="product-742930153515">
										<div class="products product-">

											<div class="product-container">









												<a href="../products/product-17.html" class="grid-link">




													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url('assets/');?>cdn.shopify.com/s/files/1/2721/6956/products/product63744.jpg?v=1518071620"
															class="featured-image" alt="Masonry equipments">
														</span> <img
															src="<?php echo base_url('assets/');?>cdn.shopify.com/s/files/1/2721/6956/products/product9_52ab4923-d525-4bef-b055-0cb554d3eb0cbbbc.jpg?v=1518083574"
															class="hidden-feature_img" alt="Masonry equipments" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">



														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930153515">
															<input type="hidden" name="id" value="10354989891627" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>







														<a href="javascript:void(0)" id="product-17"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>



														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-17 loading">
																	<a class="add-in-wishlist-js" href="product-17.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-17 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-17.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-17 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="../pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>



														<a href="#" class="compare action-home4"
															data-pid="product-17" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="../products/product-17.html"
													class="grid-link__title">Masonry equipments</a>


												<div class="grid-link__meta">
													<div class="product_price">


														<div class="grid-link__org_price">
															<span class=money>$350.00</span>
														</div>


													</div>


												</div>

											</div>


										</div>
									</li>


								</ul>

							</div>
						</div>
					</div>
				</div>

			</div>
		</div>

	</div>


</div>


<div class="dt-sc-hr-invisible-large"></div>
