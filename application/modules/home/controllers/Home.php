<?php
class Home extends MY_Controller
{
    private $template;
    public function __construct()
    {
        parent::__construct();
        $this->template = "template/index";
    }
    
    public function index(){
        $this->data['content'] = 'home';
        $this->data['title'] = 'Home';
        $this->_render_page($this->template, $this->data);
    }
    
    public function about(){
        $this->data['content'] = 'about';
        $this->data['title'] = 'About';
        $this->_render_page($this->template, $this->data);
    }
    
    public function contact(){
        $this->data['content'] = 'contact';
        $this->data['title'] = 'Contact Us';
        $this->_render_page($this->template, $this->data);
    }
    
    public function gallery($type = ""){
        $this->data['content'] = $type."_gallery";
        $this->data['title'] = "$type Galler";
        $this->_render_page($this->template, $this->data);
    }
    
    public function terms(){
        $this->data['content'] = "terms";
        $this->data['title'] = "Orders & Returns";
        $this->_render_page($this->template, $this->data);
    }
}

