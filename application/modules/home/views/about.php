<style>



  .about-us  .block7 h2 {color: #fab915;}

  .about-us  .block7 h5 {color: #434343;}

  .about-us   .block7 p {color: #999999;}

  .about-us   .block7 .mid-grid-data-details h3, .about-us .block7 .mid-grid-data-details h5  {color: #202447;}

  .about-us  .block7 .mid-grid-data-details p,.about-us  .block7 .mid-grid-data-details h6  {color: #202447;}

  .about-us .block7 .number-icon {border-bottom: 1px dashed #434343;}

  .about-us .block7 .mid-grid-data-details {background:#fab915;}
  
  .about-us  .section-six  .gallery-overlay{   background-color:rgba(250, 185, 21, 0);}

  .about-us .section-six .gallery-img:hover .gallery-overlay{background-color:rgba(250, 185, 21, 0.5);}

  .about-us  .section-six  .overlay-link h5{color:#ffffff ;}

  .about-us  .section-six  .overlay-link h4{color:#ffffff ;}

  .about-us .team-detail {background-color:rgba(0,0,0,0.7);}

  .about-us .team-detail a { border: 1px solid #ffffff;color: #ffffff;}

  .about-us .team-detail a:hover{ border: 1px solid #fab915; color: #ffffff;background:#fab915;}

  .about-us .team h4 , .about-us .team h5{color:#ffffff;}

  .about-us .team:hover .team-detail h5,.about-us .team:hover .team-detail h4{color:#ffffff;}

  .about-us .team:hover .team-detail {background:#fab915;}

  .about-us .section-one .right-section{border-left: 1px solid rgba(0,0,0,0);}

  .about-us .icon-section .icon{background:#fab915;}

  .about-us .icon-section .icon{color:#ffffff;}

  .about-us .icon-section  .icon-heading h3{color:#ffffff;}

  .about-us .icon-desc p{color:#ffffff;}

  .about-us .section-one::after { background: #434343;}

  .about-us .about-btn:hover{background:#202447!important;color:#ffffff!important;}
</style>

<div class="dt-sc-hr-invisible-large"></div>
          
      
        <div class="grid__item">  
          
          <div class="grid">
  <div class="grid__item">	
      <div id="shopify-section-about" class="shopify-section"><div data-section-id="about"  data-section-type="About section" class="about-us">
  
  <div class="section-one" style="background-image:url(<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/img289b48.jpg?v=1519368631);background-attachment:fixed;background-position:center;">
    <div class="grid__item">
      <div class="grid__item wide--one-half post-large--one-half large--one-whole left-section"> 
        <div class="title">
          <span style="color:#434343;background:#ffffff;">our</span> 
          <span  class="title-two"   style="color:#ffffff;background:#fab915;">work</span> 
        </div>
        
        <p style="color:#ffffff;">We are the experts in providing Howzzat UPVC Windows and Doors Hardware in Pollachi, Tamilnadu, India. UPVC specialize in providing you with a vast variety of Windows and Doors Hardware and also in having the expertise to guide you, to find exactly what you want. Whether you just need hardware for a window or a door , or a complete UPVC Windows & Doors locking mechanism, if stock is available in our company we will deliver, or we will find it for you. Alternatively, we provide replacement.</p>
        <p style="color:#ffffff;">As dealers, we consider the safety and quality as important and frequently, getting the people feedback. We also give many guidelines, articles and design structure to help you to do-it- yourself projects with minimal effort. If the article or design structure you are looking for is not available in our website, contact us and we will provide you.</p>
        <p style="color:#ffffff;">Introduce new design structure and using new technology. We are the best in offering PVC Windows & Doors Hardware, roller, pop-up handle, touch lock, etc,. We also provide guarantee and maintenance service also.</p>
        <div class="invisible-very-small"></div>
         
        
        <!-- <a href="#"  class="about-btn" style="color:#ffffff;background:#fab915;">Read More</a> -->
         
      </div>
      <div class="grid__item wide--one-half post-large--one-half large--one-whole right-section"> 
        <div class="grid__item">
          <ul>
            <li>
              
              <div class="grid__item wide--one-half post-large--one-half large--one-half">
                <div class="icon-section">
                  <div class="icon-heading">
                    
                    <span class="icon">
                      <i class="fa fa-thumbs-up" aria-hidden="true"></i>
                    </span>
                     
                      <h3>Our Quality</h3>  
                  </div>
                  
                  <div class="icon-desc">
                    <p>UPVC Windows and Doors Hardware, has designed and tested at each level to achieve a better quality. So that, every product can able to work for long time.</p>
                  </div>
                   
                </div>
              </div>
               
              
              <div class="grid__item wide--one-half post-large--one-half large--one-half">
                <div class="icon-section">
                  <div class="icon-heading">
                    
                    <span class="icon">
                      <i class="fa fa-check" aria-hidden="true"></i>
                    </span>
                     
                      <h3>Guarantee</h3>  
                  </div>
                  
                  <div class="icon-desc">
                    <p>UPVC Windows & Doors Hardware has a standard quality and we are providing assurance for our hardware products, services, or transactions.</p>
                  </div>
                   
                </div>
              </div>
               
            </li>
            <li>
              
              <div class="grid__item wide--one-half post-large--one-half large--one-half">
                <div class="icon-section">
                  <div class="icon-heading">
                    
                    <span class="icon">
                      <i class="fa fa-th" aria-hidden="true"></i>
                    </span>
                     
                      <h3>Collections</h3>  
                  </div>
                  
                  <div class="icon-desc">
                    <p>To attract the customers, we are giving new & more collections. Different collections have different range of price. So all people can afford our new collections.</p>
                  </div>
                   
                </div>
              </div>
               
              
              <div class="grid__item wide--one-half post-large--one-half large--one-half">
                <div class="icon-section">
                  <div class="icon-heading">
                    
                    <span class="icon">
                      <i class="fa fa-bold" aria-hidden="true"></i>
                    </span>
                     
                      <h3>Brand</h3>  
                  </div>
                  
                  <div class="icon-desc">
                    <p>Howzzat UPVC Windows & Doors Hardware dealers in Tamilnadu, India had a unique brand and maintaining the same brand from beginning to still.</p>
                  </div>
                   
                </div>
              </div>
               
            </li>
            
          </ul>
        </div>
      </div>
    </div>
  </div>
  

<div class="grid-uniform section-four">
    <div class="container">
      
      <div class="grid__item">
        
        <div class="grid__item wide--one-quarter post-large--one-quarter large--one-half medium--one-half small-grid__item "> 
          <div class="team">
            
            <div class="team-img">
              <img src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/team12fe5.jpg?v=1519369229" alt="" />
            </div>
            
            <div class="team-detail">
              
              <h4>Rachel Holloway</h4>
              

              
              <h5>Landscaper</h5>
              
              <ul>
                
                <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                
                
                <li><a href="#"><i class="fab fa-facebook" aria-hidden="true"></i></a></li>
                
                
                <li><a href="#"><i class="fab fa-google-plus" aria-hidden="true"></i></a></li>
                
                
                <li><a href="#"><i class="fab fa-linkedin" aria-hidden="true"></i></a></li>
                
              </ul>
            </div>
          </div>
        </div>
         

        
        <div class="grid__item wide--one-quarter post-large--one-quarter large--one-half medium--one-half small-grid__item "> 
          <div class="team">
            
            <div class="team-img">
              <img src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/team2eb27.jpg?v=1519369232" alt="" />
            </div>
            
            <div class="team-detail">
              
              <h4>Walter Chambers</h4>
              

              
              <h5>Architecht</h5>
              
              <ul>
                
                <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                
                
                <li><a href="#"><i class="fab fa-facebook" aria-hidden="true"></i></a></li>
                
                
                <li><a href="#"><i class="fab fa-google-plus" aria-hidden="true"></i></a></li>
                
                
                <li><a href="#"><i class="fab fa-linkedin" aria-hidden="true"></i></a></li>
                
              </ul>
            </div>
          </div>
        </div>
         

        
        <div class="grid__item wide--one-quarter post-large--one-quarter large--one-half medium--one-half small-grid__item "> 
          <div class="team">
            
            <div class="team-img">
              <img src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/team36e5f.jpg?v=1519369237" alt="" />
            </div>
            
            <div class="team-detail">
              
              <h4>Shirley Mckenzie</h4>
              

              
              <h5>Gardener</h5>
              
              <ul>
                
                <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                
                
                <li><a href="#"><i class="fab fa-facebook" aria-hidden="true"></i></a></li>
                
                
                <li><a href="#"><i class="fab fa-google-plus" aria-hidden="true"></i></a></li>
                
                
                <li><a href="#"><i class="fab fa-linkedin" aria-hidden="true"></i></a></li>
                
              </ul>
            </div>
          </div>
        </div>
         

        
        <div class="grid__item wide--one-quarter post-large--one-quarter large--one-half medium--one-half small-grid__item "> 
          <div class="team">
            
            <div class="team-img">
              <img src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/team4fc5f.jpg?v=1519369240" alt="" />
            </div>
            
            <div class="team-detail">
              
              <h4>Tommie Carroll</h4>
              

              
              <h5>Plumber</h5>
              
              <ul>
                
                <li><a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
                
                
                <li><a href="#"><i class="fab fa-facebook" aria-hidden="true"></i></a></li>
                
                
                <li><a href="#"><i class="fab fa-google-plus" aria-hidden="true"></i></a></li>
                
                
                <li><a href="#"><i class="fab fa-linkedin" aria-hidden="true"></i></a></li>
                
              </ul>
            </div>
          </div>
        </div>
         
      </div>
    </div>
  </div>

  
  
  <div class="grid-uniform client-section">
    <div class="container">
      
      <div class="main-title">
        <h2 style="color:#483b33">Our Clients</h2>
      </div>
      
      <div class="grid__item">
        
        <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item "> 
          
          <div class="client-img">
            <a href="#">  <img src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/brand5_c42410ec-ac41-4a60-9d00-6dfeba7d4a5a7c61.png?v=1518010266" alt="" /></a>
            
          </div>
        </div>
         
        
        <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item "> 
          
          <div class="client-img">
            <a href="#">  <img src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/brand36b76.png?v=1518010277" alt="" /></a>
            
          </div>
        </div>
         
        
        <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item "> 
          
          <div class="client-img">
            <a href="#">  <img src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/brand2b13c.png?v=1518010292" alt="" /></a>
            
          </div>
        </div>
         
        
        <div class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half small-grid__item "> 
          
          <div class="client-img">
            <a href="#">  <img src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/brand1f89d.png?v=1518010300" alt="" /></a>
            
          </div>
        </div>
         
      </div>
    </div>
  </div>
   
</div>








































</div>
  </div>
</div>
		          
        </div>  
