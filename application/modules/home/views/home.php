<div class="grid__item">

		<div id="shopify-section-slideshow"
			class="shopify-section index-section index-section--flush">

			<div class="home-slideshow">
				<div class="variable-width"
					data-slick='{
      dots: true,
      slidesToScroll: 1,
      autoplay:true,
      fade: true,
      autoplaySpeed:3000
	  }'>

					<div class="slick-list slideshow__slide--1515664323461 ">

						<a href="#"><img
							src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/bg3_2000xd277.jpg?v=1522671448"
							data-url="" class="slide-img " alt="" /> </a>

						<div class="slider-content slider-content-1-bg"
							style="left: 5%; top: 60%; right: initial !important;">

							<p class="slide-text animated "
								style="font-size: 40px; color: #202447;">Need A Professional</p>
							<h2 class="slide-heading animated "
								style="font-size: 56px; color: #202447;">Plumbing Service?</h2>
							<a href="#" class="slide-button animated btn "> Shop Now </a>

						</div>

					</div>

					<div class="slick-list slideshow__slide--1515665251064 ">

						<a href="#"><img
							src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/bg2_2000x89a9.jpg?v=1522671468"
							data-url="" class="slide-img " alt="" /> </a>

						<div class="slider-content slider-content-2-bg"
							style="right: 0%; text-align: center; left: 0;">

							<p class="slide-text animated "
								style="font-size: 40px; color: #202447;">Need A Plumber?</p>
							<h2 class="slide-heading animated "
								style="font-size: 56px; color: #202447;">Call 813-872-0100</h2>
							<a href="#" class="slide-button animated btn "> Shop Now </a>

						</div>

					</div>

					<div class="slick-list slideshow__slide--1518003417415 ">

						<a href="collections/all.html"><img
							src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/bg1_2000xd236.jpg?v=1522671481"
							data-url="/collections/all" class="slide-img " alt="" /> </a>

						<div class="slider-content slider-content-3-bg"
							style="left: 5%; top: 60%; right: initial !important;">

							<p class="slide-text animated "
								style="font-size: 40px; color: #202447;">Any plumbing service</p>
							<h2 class="slide-heading animated "
								style="font-size: 56px; color: #202447;">$40 Off all products</h2>
							<a href="collections/all.html" class="slide-button animated btn ">

								Shop Now </a>

						</div>

					</div>

				</div>
			</div>

			<style type="text/css">
</style>
		</div>
		<div class="dt-sc-hr-invisible-large"></div>

		<!-- BEGIN content_for_index -->
		<div id="shopify-section-1515665844410"
			class="shopify-section index-section">
			<div data-section-id="1515665844410"
				data-section-type="home-product-grid-type-9"
				class="home-product-grid-type-9">
				<div class="load-wrapper container">
					<div class="grid__item">

						<div class="border-title wow bounce">
							<h2 class="section-header__title" style="color: #434343;">
								Featured Products</h2>
						</div>

						<div class="nav-type9__items 1515665844410"></div>
					</div>
					<div class="grid-uniform">

						<div
							class="grid__item wow fadeInLeft wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item image-on-left">
							<div class="ovrly29">
								<div class="collection_title"></div>

								<img
									src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/img7_64d1ecdf-d2f8-4f35-aa56-41e08f59bbcc9ac7.jpg?v=1518005545"
									alt="files/img7_64d1ecdf-d2f8-4f35-aa56-41e08f59bbcc.jpg" />
								<div class="ovrly"></div>

							</div>
						</div>

						<div
							class="grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item">
							<div
								class="loop-item  type9__items  1515665844410 owl-carousel owl-theme">
								<ul class="type9__inner">

									<li
										class="grid__item wow fadeInUp item-row  1  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item  on-sale"
										id="product-742931431467">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-29.html"
													class="grid-link">

													<div class="featured-tag">
														<span class="badge badge--sale"> <span
															class="gift-tag badge__text">Sale</span>
														</span>
													</div>

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product1_largec027.jpg?v=1518071348"
															class="featured-image" alt="Reducers &amp; Adapters">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product32_9b1e0e9a-d130-4c20-b1ef-2ae3183d314d_large339e.jpg?v=1518073854"
															class="hidden-feature_img" alt="Reducers &amp; Adapters" />
													</div>

												</a>
												<div class="product_right_tag   offer_exist "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742931431467">
															<input type="hidden" name="id" value="10348139118635" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-29"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-29 loading">
																	<a class="add-in-wishlist-js" href="product-29.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-29 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-29.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-29 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-29" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-29.html"
													class="grid-link__title">Reducers & Adapters</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$300.00</span>
														</div>

														<del class="grid-link__sale_price">
															<span class=money>$500.00</span>
														</del>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  2  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742931333163">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-28.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product19_largee305.jpg?v=1518087792"
															class="featured-image" alt="The Vise">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product18_70da4d32-c08f-47b9-9bf1-f0ec27b87dc0_large1f0e.jpg?v=1518087819"
															class="hidden-feature_img" alt="The Vise" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742931333163">
															<input type="hidden" name="id" value="8508820783147" /> <a
																class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-28"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-28 loading">
																	<a class="add-in-wishlist-js" href="product-28.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-28 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-28.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-28 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-28" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-28.html"
													class="grid-link__title">The Vise</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$753.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  3  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742931202091">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-27.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product22_large9245.jpg?v=1518072070"
															class="featured-image" alt="Hacksaw Blades Kit">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product12_31bd55cf-6146-46ae-a11c-8fc2f076edde_largebce2.jpg?v=1518087563"
															class="hidden-feature_img" alt="Hacksaw Blades Kit" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742931202091">
															<input type="hidden" name="id" value="8508820422699" /> <a
																class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-27"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-27 loading">
																	<a class="add-in-wishlist-js" href="product-27.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-27 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-27.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-27 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-27" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-27.html"
													class="grid-link__title">Hacksaw Blades Kit</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$389.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  4  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742931038251">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-26.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product23_largea3ce.jpg?v=1518072045"
															class="featured-image" alt="Monkey Spanners">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product28_b6beb2f3-9b6f-4918-9529-6a27a19ecfc4_largea03a.jpg?v=1518087306"
															class="hidden-feature_img" alt="Monkey Spanners" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742931038251">
															<input type="hidden" name="id" value="8508819439659" /> <a
																class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-26"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-26 loading">
																	<a class="add-in-wishlist-js" href="product-26.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-26 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-26.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-26 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-26" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-26.html"
													class="grid-link__title">Monkey Spanners</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$367.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

								</ul>
								<ul class="type9__inner 0">

									<li
										class="grid__item wow fadeInUp item-row  5  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742930972715">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-25.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product26_largeec91.jpg?v=1518086938"
															class="featured-image" alt="Hammer Set">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product25_a0bc1aca-1422-482e-a99e-611a9d2cc140_large0523.jpg?v=1518086948"
															class="hidden-feature_img" alt="Hammer Set" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930972715">
															<input type="hidden" name="id" value="10357560999979" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-25"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-25 loading">
																	<a class="add-in-wishlist-js" href="product-25.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-25 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-25.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-25 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-25" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-25.html"
													class="grid-link__title">Hammer Set</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$423.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  6  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742930907179">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-24.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product32_largecf18.jpg?v=1518071999"
															class="featured-image" alt="Clamps &amp; Reducers">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product31_large06a9.jpg?v=1518086562"
															class="hidden-feature_img" alt="Clamps &amp; Reducers" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930907179">
															<input type="hidden" name="id" value="8508817342507" /> <a
																class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-24"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-24 loading">
																	<a class="add-in-wishlist-js" href="product-24.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-24 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-24.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-24 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-24" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-24.html"
													class="grid-link__title">Clamps & Reducers</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$200.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  7  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item  on-sale"
										id="product-742930874411">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-23.html"
													class="grid-link">

													<div class="featured-tag">
														<span class="badge badge--sale"> <span
															class="gift-tag badge__text">Sale</span>
														</span>
													</div>

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product11_large07cf.jpg?v=1518071990"
															class="featured-image" alt="Cutting Pliers Set II">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product15_cbbfd503-5aad-4d1f-b47f-8ff8c3af1cf7_largeca99.jpg?v=1518086366"
															class="hidden-feature_img" alt="Cutting Pliers Set II" />
													</div>

												</a>
												<div class="product_right_tag   offer_exist "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930874411">
															<input type="hidden" name="id" value="8508816752683" /> <a
																class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-23"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-23 loading">
																	<a class="add-in-wishlist-js" href="product-23.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-23 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-23.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-23 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-23" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-23.html"
													class="grid-link__title">Cutting Pliers Set II</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$399.00</span>
														</div>

														<del class="grid-link__sale_price">
															<span class=money>$600.00</span>
														</del>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  8  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742930710571">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-22.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product30_largeaef3.jpg?v=1518071982"
															class="featured-image" alt="Carpentry tools set">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product28_large54ed.jpg?v=1518085299"
															class="hidden-feature_img" alt="Carpentry tools set" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930710571">
															<input type="hidden" name="id" value="8508816523307" /> <a
																class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-22"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-22 loading">
																	<a class="add-in-wishlist-js" href="product-22.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-22 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-22.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-22 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-22" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-22.html"
													class="grid-link__title">Carpentry tools set</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$753.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

								</ul>
								<ul class="type9__inner 0">

									<li
										class="grid__item wow fadeInUp item-row  9  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item  on-sale"
										id="product-742930645035">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-21.html"
													class="grid-link">

													<div class="featured-tag">
														<span class="badge badge--sale"> <span
															class="gift-tag badge__text">Sale</span>
														</span>
													</div>

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product4_large84bd.jpg?v=1518071901"
															class="featured-image" alt="Cutting Pliers Set">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product15_52e33829-bf2d-4ec4-a4c7-272564e64e09_large1dda.jpg?v=1518084989"
															class="hidden-feature_img" alt="Cutting Pliers Set" />
													</div>

												</a>
												<div class="product_right_tag   offer_exist "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930645035">
															<input type="hidden" name="id" value="8508816392235" /> <a
																class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-21"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-21 loading">
																	<a class="add-in-wishlist-js" href="product-21.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-21 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-21.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-21 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-21" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-21.html"
													class="grid-link__title">Cutting Pliers Set</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$300.00</span>
														</div>

														<del class="grid-link__sale_price">
															<span class=money>$400.00</span>
														</del>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  10  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742930579499">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-20.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product27_large85fe.jpg?v=1518084601"
															class="featured-image" alt="Safety Kit">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product30_292dc4f8-7f24-4725-bfdf-e84e0f748112_large5a78.jpg?v=1518084611"
															class="hidden-feature_img" alt="Safety Kit" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930579499">
															<input type="hidden" name="id" value="10355706232875" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-20"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-20 loading">
																	<a class="add-in-wishlist-js" href="product-20.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-20 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-20.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-20 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-20" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-20.html"
													class="grid-link__title">Safety Kit</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$670.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  11  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742930513963">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-19.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product24_large4d02.jpg?v=1518071856"
															class="featured-image" alt="Spanner Set">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product29_3799942d-c315-4605-b718-37097ea9de77_large7574.jpg?v=1518084323"
															class="hidden-feature_img" alt="Spanner Set" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930513963">
															<input type="hidden" name="id" value="10355463356459" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-19"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-19 loading">
																	<a class="add-in-wishlist-js" href="product-19.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-19 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-19.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-19 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-19" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-19.html"
													class="grid-link__title">Spanner Set</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$400.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  12  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item  on-sale"
										id="product-742930415659">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-18.html"
													class="grid-link">

													<div class="featured-tag">
														<span class="badge badge--sale"> <span
															class="gift-tag badge__text">Sale</span>
														</span>
													</div>

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product29_large608f.jpg?v=1518071840"
															class="featured-image" alt="Spanner Kit">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product24_66ed77b6-85cb-4683-a232-7229b007e17b_large7549.jpg?v=1518083984"
															class="hidden-feature_img" alt="Spanner Kit" />
													</div>

												</a>
												<div class="product_right_tag   offer_exist "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930415659">
															<input type="hidden" name="id" value="10355209044011" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-18"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-18 loading">
																	<a class="add-in-wishlist-js" href="product-18.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-18 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-18.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-18 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-18" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-18.html"
													class="grid-link__title">Spanner Kit</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$589.00</span>
														</div>

														<del class="grid-link__sale_price">
															<span class=money>$780.00</span>
														</del>

													</div>

												</div>

											</div>

										</div>
									</li>

								</ul>
								<ul class="type9__inner 0">

									<li
										class="grid__item wow fadeInUp item-row  13  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742930153515">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-17.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product6_large3744.jpg?v=1518071620"
															class="featured-image" alt="Masonry equipments">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product9_52ab4923-d525-4bef-b055-0cb554d3eb0c_largebbbc.jpg?v=1518083574"
															class="hidden-feature_img" alt="Masonry equipments" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930153515">
															<input type="hidden" name="id" value="10354989891627" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-17"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-17 loading">
																	<a class="add-in-wishlist-js" href="product-17.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-17 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-17.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-17 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-17" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-17.html"
													class="grid-link__title">Masonry equipments</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$350.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  14  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742930087979">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-16.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product25_large9157.jpg?v=1518071595"
															class="featured-image" alt="Drilling Machine">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product22_1ef27c40-1fb7-4974-918c-d2f7bc23426e_large27b6.jpg?v=1518083068"
															class="hidden-feature_img" alt="Drilling Machine" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930087979">
															<input type="hidden" name="id" value="10354571051051" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-16"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-16 loading">
																	<a class="add-in-wishlist-js" href="product-16.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-16 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-16.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-16 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-16" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-16.html"
													class="grid-link__title">Drilling Machine</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$500.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  15  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item  on-sale"
										id="product-742929924139">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-15.html"
													class="grid-link">

													<div class="featured-tag">
														<span class="badge badge--sale"> <span
															class="gift-tag badge__text">Sale</span>
														</span>
													</div>

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product18_largef17f.jpg?v=1518071570"
															class="featured-image" alt="Hardware Starter kit">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product30_efaa5cbd-99df-44b7-901e-b9970e737513_large2297.jpg?v=1518082211"
															class="hidden-feature_img" alt="Hardware Starter kit" />
													</div>

												</a>
												<div class="product_right_tag   offer_exist "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742929924139">
															<input type="hidden" name="id" value="10354033885227" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-15"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-15 loading">
																	<a class="add-in-wishlist-js" href="product-15.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-15 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-15.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-15 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-15" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-15.html"
													class="grid-link__title">Hardware Starter kit</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$598.00</span>
														</div>

														<del class="grid-link__sale_price">
															<span class=money>$789.00</span>
														</del>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  16  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742929760299">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-14.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product13_large6fe6.jpg?v=1518081873"
															class="featured-image" alt="Steel hacksaw blades">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product25_e7332a99-903e-4f21-a0ba-2a9be8ec6fc1_large6fe6.jpg?v=1518081873"
															class="hidden-feature_img" alt="Steel hacksaw blades" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742929760299">
															<input type="hidden" name="id" value="10353633755179" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-14"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-14 loading">
																	<a class="add-in-wishlist-js" href="product-14.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-14 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-14.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-14 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-14" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-14.html"
													class="grid-link__title">Steel hacksaw blades</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$540.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

								</ul>
								<ul class="type9__inner 0">

									<li
										class="grid__item wow fadeInUp item-row  17  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item  on-sale"
										id="product-742929629227">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-13.html"
													class="grid-link">

													<div class="featured-tag">
														<span class="badge badge--sale"> <span
															class="gift-tag badge__text">Sale</span>
														</span>
													</div>

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product10_large7faa.jpg?v=1518071526"
															class="featured-image" alt="Paint Brush">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product9_de31a803-d3a5-42fa-bd36-8784b52e119e_large3a26.jpg?v=1518079092"
															class="hidden-feature_img" alt="Paint Brush" />
													</div>

												</a>
												<div class="product_right_tag   offer_exist "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742929629227">
															<input type="hidden" name="id" value="10351610626091" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-13"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-13 loading">
																	<a class="add-in-wishlist-js" href="product-13.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-13 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-13.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-13 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-13" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-13.html"
													class="grid-link__title">Paint Brush</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$400.00</span>
														</div>

														<del class="grid-link__sale_price">
															<span class=money>$897.00</span>
														</del>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  18  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742929498155">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-12.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product8_large4f4e.jpg?v=1518071484"
															class="featured-image" alt="A pair of brushes">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product9_large0c7e.jpg?v=1518077447"
															class="hidden-feature_img" alt="A pair of brushes" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742929498155">
															<input type="hidden" name="id" value="10350475903019" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-12"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-12 loading">
																	<a class="add-in-wishlist-js" href="product-12.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-12 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-12.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-12 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-12" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-12.html"
													class="grid-link__title">A pair of brushes</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$300.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  19  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742929334315">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-11.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product5_largee640.jpg?v=1518077110"
															class="featured-image" alt="Hammer">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product21_largee640.jpg?v=1518077110"
															class="hidden-feature_img" alt="Hammer" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742929334315">
															<input type="hidden" name="id" value="10350283063339" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-11"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-11 loading">
																	<a class="add-in-wishlist-js" href="product-11.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-11 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-11.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-11 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-11" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-11.html"
													class="grid-link__title">Hammer</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$500.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  20  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742931464235">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-1.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product3_largeaedb.jpg?v=1518071411"
															class="featured-image" alt="Wrench Set">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product15_large0a1a.jpg?v=1518074581"
															class="hidden-feature_img" alt="Wrench Set" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742931464235">
															<input type="hidden" name="id" value="10348659081259" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-1"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-1 loading">
																	<a class="add-in-wishlist-js" href="product-1.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-1 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-1.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-1 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-1" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-1.html"
													class="grid-link__title">Wrench Set</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$330.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

								</ul>
								<ul class="type9__inner 0">

									<li
										class="grid__item wow fadeInUp item-row  21  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item  sold-out"
										id="product-742929203243">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-10.html"
													class="grid-link"> <span class="badge badge--sold-out"> <span
														class="badge__text">Sold Out</span>
												</span>

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product2_large5bbc.jpg?v=1518071378"
															class="featured-image" alt="9 meters inch tap">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product22_3819f9fe-06f5-40a8-b7cb-8da6d8d94f7a_large1bc5.jpg?v=1518075700"
															class="hidden-feature_img" alt="9 meters inch tap" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<a href="javascript:void(0)" id="product-10"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-10 loading">
																	<a class="add-in-wishlist-js" href="product-10.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-10 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-10.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-10 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-10" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-10.html"
													class="grid-link__title">9 meters inch tap</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$350.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

								</ul>
							</div>
						</div>

					</div>
					<div class="dt-sc-hr-invisible-large"></div>
				</div>
			</div>

		</div>
		<div id="shopify-section-1518002029518"
			class="shopify-section index-section">
			<div data-section-id="1518002029518"
				data-section-type="about-circle-type" class="about-circle-type">

				<div class="grid__item">

					<div class="about-circle-content" style="background-image:url(<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/img6_3ec92178-05f7-4350-9241-f240c40f0285c1f1.jpg?v=1518071758);">
						<div class="container"></div>
						<div class="overlay_bg" style="background: #000;"></div>
					</div>

					<div class="about-icon-type">
						<div class="container">

							<div
								class="grid__item wow fadeInUp wide--one-third post-large--one-third large--one-third medium--one-whole small-grid__item">
								<div class="icon-img-wrapper">

									<div class="icon-img">
										<img
											src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/img7_ede20d82-5d47-4db0-b171-9bb940cc8f32_1024x1024dbfa.jpg?v=1518072185"
											alt="" title="" style="border: 5px solid #fab915" />
									</div>

									<h4 style="color: Installation;">Installation</h4>

									<p style="color: #999999;">Nullam dictum felis eu pede mollis
										pretium. Integer tincidunt. Cras dapibus. Vivamus elementum
										semper nisi. Aenean vulputate</p>

								</div>
							</div>

							<div
								class="grid__item wow fadeInUp wide--one-third post-large--one-third large--one-third medium--one-whole small-grid__item">
								<div class="icon-img-wrapper">

									<div class="icon-img">
										<img
											src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/img8_2da0a064-f1ac-491e-b891-00db36477866_1024x10248739.jpg?v=1518072188"
											alt="" title="" style="border: 5px solid #fab915" />
									</div>

									<h4 style="color: Sanitary;">Sanitary</h4>

									<p style="color: #999999;">Aenean vulputate Integer tincidunt.
										Cras dapibus. Nullam dictum felis eu pede mollis
										pretium.Vivamus elementum semper nisi.</p>

								</div>
							</div>

							<div
								class="grid__item wow fadeInUp wide--one-third post-large--one-third large--one-third medium--one-whole small-grid__item">
								<div class="icon-img-wrapper">

									<div class="icon-img">
										<img
											src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/img9_1024x1024ecd0.jpg?v=1518072193"
											alt="" title="" style="border: 5px solid #fab915" />
									</div>

									<h4 style="color: Products;">Products</h4>

									<p style="color: #999999;">Vivamus elementum semper nisi.
										Aenean vulputate.Nullam dictum felis eu pede mollis pretium.
										Integer tincidunt. Cras dapibus.</p>

								</div>
							</div>

						</div>
					</div>
				</div>
				<div class="dt-sc-hr-invisible-large"></div>
			</div>


		</div>
		<div id="shopify-section-1515670483401"
			class="shopify-section index-section">
			<div data-section-id="1515670483401"
				data-section-type="home-product-grid-type-9"
				class="home-product-grid-type-9">
				<div class="load-wrapper container">
					<div class="grid__item">

						<div class="border-title wow bounce">
							<h2 class="section-header__title" style="color: #434343;">New
								Arrivals</h2>
						</div>

						<div class="nav-type9__items 1515670483401"></div>
					</div>
					<div class="grid-uniform">

						<div
							class="grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item">
							<div
								class="loop-item  type9__items  1515670483401 owl-carousel owl-theme">
								<ul class="type9__inner">

									<li
										class="grid__item wow fadeInUp item-row  1  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item  on-sale"
										id="product-742931431467">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-29.html"
													class="grid-link">

													<div class="featured-tag">
														<span class="badge badge--sale"> <span
															class="gift-tag badge__text">Sale</span>
														</span>
													</div>

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product1_largec027.jpg?v=1518071348"
															class="featured-image" alt="Reducers &amp; Adapters">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product32_9b1e0e9a-d130-4c20-b1ef-2ae3183d314d_large339e.jpg?v=1518073854"
															class="hidden-feature_img" alt="Reducers &amp; Adapters" />
													</div>

												</a>
												<div class="product_right_tag   offer_exist "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742931431467">
															<input type="hidden" name="id" value="10348139118635" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-29"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-29 loading">
																	<a class="add-in-wishlist-js" href="product-29.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-29 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-29.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-29 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-29" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-29.html"
													class="grid-link__title">Reducers & Adapters</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$300.00</span>
														</div>

														<del class="grid-link__sale_price">
															<span class=money>$500.00</span>
														</del>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  2  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742931333163">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-28.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product19_largee305.jpg?v=1518087792"
															class="featured-image" alt="The Vise">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product18_70da4d32-c08f-47b9-9bf1-f0ec27b87dc0_large1f0e.jpg?v=1518087819"
															class="hidden-feature_img" alt="The Vise" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742931333163">
															<input type="hidden" name="id" value="8508820783147" /> <a
																class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-28"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-28 loading">
																	<a class="add-in-wishlist-js" href="product-28.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-28 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-28.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-28 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-28" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-28.html"
													class="grid-link__title">The Vise</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$753.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  3  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742931202091">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-27.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product22_large9245.jpg?v=1518072070"
															class="featured-image" alt="Hacksaw Blades Kit">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product12_31bd55cf-6146-46ae-a11c-8fc2f076edde_largebce2.jpg?v=1518087563"
															class="hidden-feature_img" alt="Hacksaw Blades Kit" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742931202091">
															<input type="hidden" name="id" value="8508820422699" /> <a
																class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-27"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-27 loading">
																	<a class="add-in-wishlist-js" href="product-27.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-27 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-27.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-27 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-27" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-27.html"
													class="grid-link__title">Hacksaw Blades Kit</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$389.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  4  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742931038251">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-26.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product23_largea3ce.jpg?v=1518072045"
															class="featured-image" alt="Monkey Spanners">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product28_b6beb2f3-9b6f-4918-9529-6a27a19ecfc4_largea03a.jpg?v=1518087306"
															class="hidden-feature_img" alt="Monkey Spanners" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742931038251">
															<input type="hidden" name="id" value="8508819439659" /> <a
																class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-26"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-26 loading">
																	<a class="add-in-wishlist-js" href="product-26.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-26 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-26.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-26 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-26" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-26.html"
													class="grid-link__title">Monkey Spanners</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$367.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

								</ul>
								<ul class="type9__inner 0">

									<li
										class="grid__item wow fadeInUp item-row  5  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742930972715">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-25.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product26_largeec91.jpg?v=1518086938"
															class="featured-image" alt="Hammer Set">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product25_a0bc1aca-1422-482e-a99e-611a9d2cc140_large0523.jpg?v=1518086948"
															class="hidden-feature_img" alt="Hammer Set" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930972715">
															<input type="hidden" name="id" value="10357560999979" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-25"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-25 loading">
																	<a class="add-in-wishlist-js" href="product-25.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-25 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-25.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-25 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-25" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-25.html"
													class="grid-link__title">Hammer Set</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$423.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  6  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742930907179">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-24.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product32_largecf18.jpg?v=1518071999"
															class="featured-image" alt="Clamps &amp; Reducers">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product31_large06a9.jpg?v=1518086562"
															class="hidden-feature_img" alt="Clamps &amp; Reducers" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930907179">
															<input type="hidden" name="id" value="8508817342507" /> <a
																class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-24"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-24 loading">
																	<a class="add-in-wishlist-js" href="product-24.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-24 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-24.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-24 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-24" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-24.html"
													class="grid-link__title">Clamps & Reducers</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$200.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  7  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item  on-sale"
										id="product-742930874411">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-23.html"
													class="grid-link">

													<div class="featured-tag">
														<span class="badge badge--sale"> <span
															class="gift-tag badge__text">Sale</span>
														</span>
													</div>

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product11_large07cf.jpg?v=1518071990"
															class="featured-image" alt="Cutting Pliers Set II">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product15_cbbfd503-5aad-4d1f-b47f-8ff8c3af1cf7_largeca99.jpg?v=1518086366"
															class="hidden-feature_img" alt="Cutting Pliers Set II" />
													</div>

												</a>
												<div class="product_right_tag   offer_exist "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930874411">
															<input type="hidden" name="id" value="8508816752683" /> <a
																class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-23"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-23 loading">
																	<a class="add-in-wishlist-js" href="product-23.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-23 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-23.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-23 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-23" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-23.html"
													class="grid-link__title">Cutting Pliers Set II</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$399.00</span>
														</div>

														<del class="grid-link__sale_price">
															<span class=money>$600.00</span>
														</del>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  8  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742930710571">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-22.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product30_largeaef3.jpg?v=1518071982"
															class="featured-image" alt="Carpentry tools set">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product28_large54ed.jpg?v=1518085299"
															class="hidden-feature_img" alt="Carpentry tools set" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930710571">
															<input type="hidden" name="id" value="8508816523307" /> <a
																class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-22"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-22 loading">
																	<a class="add-in-wishlist-js" href="product-22.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-22 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-22.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-22 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-22" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-22.html"
													class="grid-link__title">Carpentry tools set</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$753.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

								</ul>
								<ul class="type9__inner 0">

									<li
										class="grid__item wow fadeInUp item-row  9  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item  on-sale"
										id="product-742930645035">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-21.html"
													class="grid-link">

													<div class="featured-tag">
														<span class="badge badge--sale"> <span
															class="gift-tag badge__text">Sale</span>
														</span>
													</div>

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product4_large84bd.jpg?v=1518071901"
															class="featured-image" alt="Cutting Pliers Set">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product15_52e33829-bf2d-4ec4-a4c7-272564e64e09_large1dda.jpg?v=1518084989"
															class="hidden-feature_img" alt="Cutting Pliers Set" />
													</div>

												</a>
												<div class="product_right_tag   offer_exist "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930645035">
															<input type="hidden" name="id" value="8508816392235" /> <a
																class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-21"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-21 loading">
																	<a class="add-in-wishlist-js" href="product-21.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-21 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-21.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-21 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-21" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-21.html"
													class="grid-link__title">Cutting Pliers Set</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$300.00</span>
														</div>

														<del class="grid-link__sale_price">
															<span class=money>$400.00</span>
														</del>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  10  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742930579499">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-20.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product27_large85fe.jpg?v=1518084601"
															class="featured-image" alt="Safety Kit">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product30_292dc4f8-7f24-4725-bfdf-e84e0f748112_large5a78.jpg?v=1518084611"
															class="hidden-feature_img" alt="Safety Kit" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930579499">
															<input type="hidden" name="id" value="10355706232875" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-20"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-20 loading">
																	<a class="add-in-wishlist-js" href="product-20.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-20 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-20.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-20 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-20" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-20.html"
													class="grid-link__title">Safety Kit</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$670.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  11  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742930513963">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-19.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product24_large4d02.jpg?v=1518071856"
															class="featured-image" alt="Spanner Set">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product29_3799942d-c315-4605-b718-37097ea9de77_large7574.jpg?v=1518084323"
															class="hidden-feature_img" alt="Spanner Set" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930513963">
															<input type="hidden" name="id" value="10355463356459" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-19"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-19 loading">
																	<a class="add-in-wishlist-js" href="product-19.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-19 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-19.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-19 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-19" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-19.html"
													class="grid-link__title">Spanner Set</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$400.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  12  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item  on-sale"
										id="product-742930415659">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-18.html"
													class="grid-link">

													<div class="featured-tag">
														<span class="badge badge--sale"> <span
															class="gift-tag badge__text">Sale</span>
														</span>
													</div>

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product29_large608f.jpg?v=1518071840"
															class="featured-image" alt="Spanner Kit">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product24_66ed77b6-85cb-4683-a232-7229b007e17b_large7549.jpg?v=1518083984"
															class="hidden-feature_img" alt="Spanner Kit" />
													</div>

												</a>
												<div class="product_right_tag   offer_exist "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930415659">
															<input type="hidden" name="id" value="10355209044011" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-18"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-18 loading">
																	<a class="add-in-wishlist-js" href="product-18.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-18 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-18.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-18 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-18" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-18.html"
													class="grid-link__title">Spanner Kit</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$589.00</span>
														</div>

														<del class="grid-link__sale_price">
															<span class=money>$780.00</span>
														</del>

													</div>

												</div>

											</div>

										</div>
									</li>

								</ul>
								<ul class="type9__inner 0">

									<li
										class="grid__item wow fadeInUp item-row  13  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742930153515">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-17.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product6_large3744.jpg?v=1518071620"
															class="featured-image" alt="Masonry equipments">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product9_52ab4923-d525-4bef-b055-0cb554d3eb0c_largebbbc.jpg?v=1518083574"
															class="hidden-feature_img" alt="Masonry equipments" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930153515">
															<input type="hidden" name="id" value="10354989891627" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-17"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-17 loading">
																	<a class="add-in-wishlist-js" href="product-17.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-17 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-17.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-17 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-17" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-17.html"
													class="grid-link__title">Masonry equipments</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$350.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  14  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742930087979">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-16.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product25_large9157.jpg?v=1518071595"
															class="featured-image" alt="Drilling Machine">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product22_1ef27c40-1fb7-4974-918c-d2f7bc23426e_large27b6.jpg?v=1518083068"
															class="hidden-feature_img" alt="Drilling Machine" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742930087979">
															<input type="hidden" name="id" value="10354571051051" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-16"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-16 loading">
																	<a class="add-in-wishlist-js" href="product-16.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-16 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-16.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-16 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-16" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-16.html"
													class="grid-link__title">Drilling Machine</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$500.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  15  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item  on-sale"
										id="product-742929924139">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-15.html"
													class="grid-link">

													<div class="featured-tag">
														<span class="badge badge--sale"> <span
															class="gift-tag badge__text">Sale</span>
														</span>
													</div>

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product18_largef17f.jpg?v=1518071570"
															class="featured-image" alt="Hardware Starter kit">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product30_efaa5cbd-99df-44b7-901e-b9970e737513_large2297.jpg?v=1518082211"
															class="hidden-feature_img" alt="Hardware Starter kit" />
													</div>

												</a>
												<div class="product_right_tag   offer_exist "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742929924139">
															<input type="hidden" name="id" value="10354033885227" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-15"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-15 loading">
																	<a class="add-in-wishlist-js" href="product-15.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-15 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-15.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-15 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-15" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-15.html"
													class="grid-link__title">Hardware Starter kit</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$598.00</span>
														</div>

														<del class="grid-link__sale_price">
															<span class=money>$789.00</span>
														</del>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  16  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742929760299">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-14.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product13_large6fe6.jpg?v=1518081873"
															class="featured-image" alt="Steel hacksaw blades">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product25_e7332a99-903e-4f21-a0ba-2a9be8ec6fc1_large6fe6.jpg?v=1518081873"
															class="hidden-feature_img" alt="Steel hacksaw blades" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742929760299">
															<input type="hidden" name="id" value="10353633755179" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-14"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-14 loading">
																	<a class="add-in-wishlist-js" href="product-14.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-14 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-14.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-14 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-14" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-14.html"
													class="grid-link__title">Steel hacksaw blades</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$540.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

								</ul>
								<ul class="type9__inner 0">

									<li
										class="grid__item wow fadeInUp item-row  17  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item  on-sale"
										id="product-742929629227">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-13.html"
													class="grid-link">

													<div class="featured-tag">
														<span class="badge badge--sale"> <span
															class="gift-tag badge__text">Sale</span>
														</span>
													</div>

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product10_large7faa.jpg?v=1518071526"
															class="featured-image" alt="Paint Brush">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product9_de31a803-d3a5-42fa-bd36-8784b52e119e_large3a26.jpg?v=1518079092"
															class="hidden-feature_img" alt="Paint Brush" />
													</div>

												</a>
												<div class="product_right_tag   offer_exist "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742929629227">
															<input type="hidden" name="id" value="10351610626091" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-13"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-13 loading">
																	<a class="add-in-wishlist-js" href="product-13.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-13 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-13.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-13 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-13" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-13.html"
													class="grid-link__title">Paint Brush</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$400.00</span>
														</div>

														<del class="grid-link__sale_price">
															<span class=money>$897.00</span>
														</del>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  18  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742929498155">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-12.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product8_large4f4e.jpg?v=1518071484"
															class="featured-image" alt="A pair of brushes">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product9_large0c7e.jpg?v=1518077447"
															class="hidden-feature_img" alt="A pair of brushes" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742929498155">
															<input type="hidden" name="id" value="10350475903019" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-12"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-12 loading">
																	<a class="add-in-wishlist-js" href="product-12.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-12 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-12.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-12 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-12" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-12.html"
													class="grid-link__title">A pair of brushes</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$300.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  19  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742929334315">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-11.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product5_largee640.jpg?v=1518077110"
															class="featured-image" alt="Hammer">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product21_largee640.jpg?v=1518077110"
															class="hidden-feature_img" alt="Hammer" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742929334315">
															<input type="hidden" name="id" value="10350283063339" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-11"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-11 loading">
																	<a class="add-in-wishlist-js" href="product-11.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-11 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-11.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-11 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-11" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-11.html"
													class="grid-link__title">Hammer</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$500.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

									<li
										class="grid__item wow fadeInUp item-row  20  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item "
										id="product-742931464235">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-1.html"
													class="grid-link">

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product3_largeaedb.jpg?v=1518071411"
															class="featured-image" alt="Wrench Set">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product15_large0a1a.jpg?v=1518074581"
															class="hidden-feature_img" alt="Wrench Set" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<form action="https://nora-demo.myshopify.com/cart/add"
															method="post" class="gom variants clearfix"
															id="cart-form-742931464235">
															<input type="hidden" name="id" value="10348659081259" />
															<a class="add-cart-btn"> <i class="icon-basket"></i>
															</a>
														</form>

														<a href="javascript:void(0)" id="product-1"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-1 loading">
																	<a class="add-in-wishlist-js" href="product-1.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-1 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-1.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-1 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-1" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-1.html"
													class="grid-link__title">Wrench Set</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$330.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

								</ul>
								<ul class="type9__inner 0">

									<li
										class="grid__item wow fadeInUp item-row  21  grid__item wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item  sold-out"
										id="product-742929203243">
										<div class="products">

											<div class="product-container">

												<a href="collections/frontpage/products/product-10.html"
													class="grid-link"> <span class="badge badge--sold-out"> <span
														class="badge__text">Sold Out</span>
												</span>

													<div class="ImageOverlayCa"></div>

													<div class="reveal">
														<span class="product-additional"> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product2_large5bbc.jpg?v=1518071378"
															class="featured-image" alt="9 meters inch tap">
														</span> <img
															src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/products/product22_3819f9fe-06f5-40a8-b7cb-8da6d8d94f7a_large1bc5.jpg?v=1518075700"
															class="hidden-feature_img" alt="9 meters inch tap" />
													</div>

												</a>
												<div class="product_right_tag  "></div>
												<div class="ImageWrapper">
													<div class="product-button">

														<a href="javascript:void(0)" id="product-10"
															class="quick-view-text"> <i class="icon-eye"
															aria-hidden="true"></i>
														</a>

														<div class="add-to-wishlist">
															<div class="show">
																<div class="default-wishbutton-product-10 loading">
																	<a class="add-in-wishlist-js" href="product-10.html"><i
																		class="icon-heart"></i><span class="tooltip-label">Add
																			to wishlist</span></a>
																</div>
																<div class="loadding-wishbutton-product-10 loading"
																	style="display: none; pointer-events: none">
																	<a class="add_to_wishlist" href="product-10.html"><i
																		class="fa fa-circle-o-notch fa-spin"></i></a>
																</div>
																<div class="added-wishbutton-product-10 loading"
																	style="display: none;">
																	<a class="added-wishlist add_to_wishlist"
																		href="pages/wishlist.html"><i class="icon-heart"></i><span
																		class="tooltip-label">View Wishlist</span></a>
																</div>
															</div>
														</div>

														<a href="#" class="compare action-home4"
															data-pid="product-10" title=""
															data-original-title="Compare product"> <i
															class="icon-chart"></i></a>

													</div>
												</div>
											</div>
											<div class="product-detail">

												<a href="collections/frontpage/products/product-10.html"
													class="grid-link__title">9 meters inch tap</a>

												<div class="grid-link__meta">
													<div class="product_price">

														<div class="grid-link__org_price">
															<span class=money>$350.00</span>
														</div>

													</div>

												</div>

											</div>

										</div>
									</li>

								</ul>
							</div>
						</div>

						<div
							class="grid__item wow fadeInRight wide--one-half post-large--one-half large--one-half medium--grid__item small-grid__item image-on-right">
							<div class="ovrly29">
								<div class="collection_title"></div>

								<img
									src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/img8a7f4.jpg?v=1518006162"
									alt="files/img8.jpg" />
								<div class="ovrly"></div>

							</div>
						</div>


					</div>
					<div class="dt-sc-hr-invisible-large"></div>
				</div>
			</div>

		</div>
		<div id="shopify-section-1518002289402"
			class="shopify-section index-section">
			<div data-section-id="1518002289402"
				data-section-type="video-content-block-1"
				class="video-content-block-1">

				<div class="grid-uniform" style="position:relative;background-image:url(<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/img110d06.jpg?v=1518073309);clear: both;background-size: cover;background-repeat: no-repeat;background-position:center;padding:100px 0;">
					<div class="video-overlay"
						style="background: #000; position: absolute; top: 0; opacity: 0.7; width: 100%; height: 100%;"></div>

					<div class="container">
						<div class="video-banner-type-1-block">

							<div
								class="intro-video grid__item wide--one-half post-large--one-half large--one-half medium--grd__item wow slideInLeft animated">
								<div class="intro-video-text">
									<a href="#inline_content" class="video-player inline"> <img
										src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/img125e59.jpg?v=1518073490"
										alt="Any plumbing service" />
									</a>
								</div>
								<div style='display: none'>
									<div id='inline_content'>
										<iframe width="980" height="500"
											src="https://www.youtube.com/embed/vgr2y70Vlk0"
											frameborder="0" allowfullscreen></iframe>
									</div>
								</div>
							</div>

							<div
								class="video-banner-type-1-content  grid__item wide--one-half post-large--one-half large--one-half medium--grd__item">

								<h2 style="color: #ffffff">Any plumbing service</h2>

								<h3 style="color: #fab915">$40 Off all products</h3>

								<a class="btn" href="#">Buy Now</a>

							</div>
						</div>
					</div>

				</div>
				<div class="dt-sc-hr-invisible-large"></div>

			</div>

		</div>
		<div id="shopify-section-1518001824511" class="shopify-section">
			<div data-section-id="1518001824511"
				data-section-type="blog-post-type-4" class="blog-post-type-4">
				<div class="container">
					<div class="blog-post"
						style="float: left; width: 100%; position: relative;">
						<div class="grid">

							<div class="section-header section-header--small">
								<div class="border-title wow swing">

									<h2 style="color: #434343;">Blog Post</h2>

								</div>
							</div>

							<div class="home-blog blog-section owl-carousel owl-theme"
								id="BlogType4">

								<div class="article-item grid__item wow fadeInUp">
									<div class="article">
										<div class="home-blog-image grid__item">

											<a href="blogs/news/blog-13.html"> <img
												src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/articles/blog12c4f2.jpg?v=1518073329"
												alt="Expert skirting of tiles done" />
											</a>

										</div>
										<div class="blog-description grid__item">
											<div class="blogs-sub-title">

												<p class="blog-date" style="color: #434343;">
													<time datetime="2018-02-08">
														<span class="date"><b style="color: #000000">08 </b> <br>
														<i> Feb</i><br> 2018 </span>
													</time>
												</p>

											</div>
											<div class="home-blog-content blog-detail"
												style="background: #f7f7f7;">

												<p class="author" style="color: #ffffff">

													<i class="lnr lnr-user"></i> <span> Ram M</span>

												</p>

												<h4>
													<a href="blogs/news/blog-13.html" style="color: #434343;">Expert
														skirting of tiles done</a>
												</h4>

												<p style="color: #999999;">Lorem ipsum dolor sit amet,
													consectetur adipisicing elit, sed do eiusmod tempor
													incididunt ut labore et dolore magna aliqua. Ut enim ad
													minim veniam, quis nostrud exer...</p>

												<div class="blog-btn">
													<a class="btn" style="color: #ffffff;"
														href="blogs/news/blog-13.html">Read more<span
														class="fas fa-long-arrow-alt-right"></span></a>
												</div>

											</div>
										</div>

									</div>

								</div>

								<div class="article-item grid__item wow fadeInUp">
									<div class="article">
										<div class="home-blog-image grid__item">

											<a href="blogs/news/blog-12.html"> <img
												src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/articles/blog111444.jpg?v=1518073310"
												alt="We are quick to fix any kind of work" />
											</a>

										</div>
										<div class="blog-description grid__item">
											<div class="blogs-sub-title">

												<p class="blog-date" style="color: #434343;">
													<time datetime="2018-02-08">
														<span class="date"><b style="color: #000000">08 </b> <br>
														<i> Feb</i><br> 2018 </span>
													</time>
												</p>

											</div>
											<div class="home-blog-content blog-detail"
												style="background: #f7f7f7;">

												<p class="author" style="color: #ffffff">

													<i class="lnr lnr-user"></i> <span> Ram M</span>

												</p>

												<h4>
													<a href="blogs/news/blog-12.html" style="color: #434343;">We
														are quick to fix any kind of work</a>
												</h4>

												<p style="color: #999999;">Lorem ipsum dolor sit amet,
													consectetur adipisicing elit, sed do eiusmod tempor
													incididunt ut labore et dolore magna aliqua. Ut enim ad
													minim veniam, quis nostrud exer...</p>

												<div class="blog-btn">
													<a class="btn" style="color: #ffffff;"
														href="blogs/news/blog-12.html">Read more<span
														class="fas fa-long-arrow-alt-right"></span></a>
												</div>

											</div>
										</div>

									</div>

								</div>

								<div class="article-item grid__item wow fadeInUp">
									<div class="article">
										<div class="home-blog-image grid__item">

											<a href="blogs/news/blog-11.html"> <img
												src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/articles/blog10245c.jpg?v=1518073280"
												alt="Checking the telephonic shower" />
											</a>

										</div>
										<div class="blog-description grid__item">
											<div class="blogs-sub-title">

												<p class="blog-date" style="color: #434343;">
													<time datetime="2018-02-08">
														<span class="date"><b style="color: #000000">08 </b> <br>
														<i> Feb</i><br> 2018 </span>
													</time>
												</p>

											</div>
											<div class="home-blog-content blog-detail"
												style="background: #f7f7f7;">

												<p class="author" style="color: #ffffff">

													<i class="lnr lnr-user"></i> <span> Ram M</span>

												</p>

												<h4>
													<a href="blogs/news/blog-11.html" style="color: #434343;">Checking
														the telephonic shower</a>
												</h4>

												<p style="color: #999999;">Lorem ipsum dolor sit amet,
													consectetur adipisicing elit, sed do eiusmod tempor
													incididunt ut labore et dolore magna aliqua. Ut enim ad
													minim veniam, quis nostrud exer...</p>

												<div class="blog-btn">
													<a class="btn" style="color: #ffffff;"
														href="blogs/news/blog-11.html">Read more<span
														class="fas fa-long-arrow-alt-right"></span></a>
												</div>

											</div>
										</div>

									</div>

								</div>

								<div class="article-item grid__item wow fadeInUp">
									<div class="article">
										<div class="home-blog-image grid__item">

											<a href="blogs/news/blog-9.html"> <img
												src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/articles/blog9c12e.jpg?v=1518073254"
												alt="The monkey spanner is versatile" />
											</a>

										</div>
										<div class="blog-description grid__item">
											<div class="blogs-sub-title">

												<p class="blog-date" style="color: #434343;">
													<time datetime="2018-02-08">
														<span class="date"><b style="color: #000000">08 </b> <br>
														<i> Feb</i><br> 2018 </span>
													</time>
												</p>

											</div>
											<div class="home-blog-content blog-detail"
												style="background: #f7f7f7;">

												<p class="author" style="color: #ffffff">

													<i class="lnr lnr-user"></i> <span> Ram M</span>

												</p>

												<h4>
													<a href="blogs/news/blog-9.html" style="color: #434343;">The
														monkey spanner is versatile</a>
												</h4>

												<p style="color: #999999;">Lorem ipsum dolor sit amet,
													consectetur adipisicing elit, sed do eiusmod tempor
													incididunt ut labore et dolore magna aliqua. Ut enim ad
													minim veniam, quis nostrud exer...</p>

												<div class="blog-btn">
													<a class="btn" style="color: #ffffff;"
														href="blogs/news/blog-9.html">Read more<span
														class="fas fa-long-arrow-alt-right"></span></a>
												</div>

											</div>
										</div>

									</div>

								</div>

								<div class="article-item grid__item wow fadeInUp">
									<div class="article">
										<div class="home-blog-image grid__item">

											<a href="blogs/news/blog-8.html"> <img
												src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/articles/blog83684.jpg?v=1518073124"
												alt="Hot & Cold water taps" />
											</a>

										</div>
										<div class="blog-description grid__item">
											<div class="blogs-sub-title">

												<p class="blog-date" style="color: #434343;">
													<time datetime="2018-02-08">
														<span class="date"><b style="color: #000000">08 </b> <br>
														<i> Feb</i><br> 2018 </span>
													</time>
												</p>

											</div>
											<div class="home-blog-content blog-detail"
												style="background: #f7f7f7;">

												<p class="author" style="color: #ffffff">

													<i class="lnr lnr-user"></i> <span> Ram M</span>

												</p>

												<h4>
													<a href="blogs/news/blog-8.html" style="color: #434343;">Hot
														& Cold water taps</a>
												</h4>

												<p style="color: #999999;">Lorem ipsum dolor sit amet,
													consectetur adipisicing elit, sed do eiusmod tempor
													incididunt ut labore et dolore magna aliqua. Ut enim ad
													minim veniam, quis nostrud exer...</p>

												<div class="blog-btn">
													<a class="btn" style="color: #ffffff;"
														href="blogs/news/blog-8.html">Read more<span
														class="fas fa-long-arrow-alt-right"></span></a>
												</div>

											</div>
										</div>

									</div>

								</div>

								<div class="article-item grid__item wow fadeInUp">
									<div class="article">
										<div class="home-blog-image grid__item">

											<a href="blogs/news/blog-7.html"> <img
												src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/articles/blog74f12.jpg?v=1518073088"
												alt="Using a monkey wrench" />
											</a>

										</div>
										<div class="blog-description grid__item">
											<div class="blogs-sub-title">

												<p class="blog-date" style="color: #434343;">
													<time datetime="2018-02-08">
														<span class="date"><b style="color: #000000">08 </b> <br>
														<i> Feb</i><br> 2018 </span>
													</time>
												</p>

											</div>
											<div class="home-blog-content blog-detail"
												style="background: #f7f7f7;">

												<p class="author" style="color: #ffffff">

													<i class="lnr lnr-user"></i> <span> Ram M</span>

												</p>

												<h4>
													<a href="blogs/news/blog-7.html" style="color: #434343;">Using
														a monkey wrench</a>
												</h4>

												<p style="color: #999999;">Lorem ipsum dolor sit amet,
													consectetur adipisicing elit, sed do eiusmod tempor
													incididunt ut labore et dolore magna aliqua. Ut enim ad
													minim veniam, quis nostrud exer...</p>

												<div class="blog-btn">
													<a class="btn" style="color: #ffffff;"
														href="blogs/news/blog-7.html">Read more<span
														class="fas fa-long-arrow-alt-right"></span></a>
												</div>

											</div>
										</div>

									</div>

								</div>

								<div class="article-item grid__item wow fadeInUp">
									<div class="article">
										<div class="home-blog-image grid__item">

											<a href="blogs/news/blog-5.html"> <img
												src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/articles/blog51ff2.jpg?v=1518072901"
												alt="The electrical circuit correction" />
											</a>

										</div>
										<div class="blog-description grid__item">
											<div class="blogs-sub-title">

												<p class="blog-date" style="color: #434343;">
													<time datetime="2018-02-08">
														<span class="date"><b style="color: #000000">08 </b> <br>
														<i> Feb</i><br> 2018 </span>
													</time>
												</p>

											</div>
											<div class="home-blog-content blog-detail"
												style="background: #f7f7f7;">

												<p class="author" style="color: #ffffff">

													<i class="lnr lnr-user"></i> <span> Ram M</span>

												</p>

												<h4>
													<a href="blogs/news/blog-5.html" style="color: #434343;">The
														electrical circuit correction</a>
												</h4>

												<p style="color: #999999;">Lorem ipsum dolor sit amet,
													consectetur adipisicing elit, sed do eiusmod tempor
													incididunt ut labore et dolore magna aliqua. Ut enim ad
													minim veniam, quis nostrud exer...</p>

												<div class="blog-btn">
													<a class="btn" style="color: #ffffff;"
														href="blogs/news/blog-5.html">Read more<span
														class="fas fa-long-arrow-alt-right"></span></a>
												</div>

											</div>
										</div>

									</div>

								</div>

								<div class="article-item grid__item wow fadeInUp">
									<div class="article">
										<div class="home-blog-image grid__item">

											<a href="blogs/news/blog-4.html"> <img
												src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/articles/blog1a7c3.jpg?v=1518069083"
												alt="Checking the electrical circuit" />
											</a>

										</div>
										<div class="blog-description grid__item">
											<div class="blogs-sub-title">

												<p class="blog-date" style="color: #434343;">
													<time datetime="2018-01-11">
														<span class="date"><b style="color: #000000">11 </b> <br>
														<i> Jan</i><br> 2018 </span>
													</time>
												</p>

											</div>
											<div class="home-blog-content blog-detail"
												style="background: #f7f7f7;">

												<p class="author" style="color: #ffffff">

													<i class="lnr lnr-user"></i> <span> Ram M</span>

												</p>

												<h4>
													<a href="blogs/news/blog-4.html" style="color: #434343;">Checking
														the electrical circuit</a>
												</h4>

												<p style="color: #999999;">Lorem ipsum dolor sit amet,
													consectetur adipiscing elit, sed do eiusmod tempor
													incididunt ut labore et dolore magna aliqua. Ut enim ad
													minim veniam, quis nostrud exerc...</p>

												<div class="blog-btn">
													<a class="btn" style="color: #ffffff;"
														href="blogs/news/blog-4.html">Read more<span
														class="fas fa-long-arrow-alt-right"></span></a>
												</div>

											</div>
										</div>

									</div>

								</div>

								<div class="article-item grid__item wow fadeInUp">
									<div class="article">
										<div class="home-blog-image grid__item">

											<a href="blogs/news/blog-3.html"> <img
												src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/articles/blog6_78bd2733-033c-4e17-a7a4-55114e6df97ae08d.jpg?v=1518069149"
												alt="Our Tech support is quick!" />
											</a>

										</div>
										<div class="blog-description grid__item">
											<div class="blogs-sub-title">

												<p class="blog-date" style="color: #434343;">
													<time datetime="2018-01-11">
														<span class="date"><b style="color: #000000">11 </b> <br>
														<i> Jan</i><br> 2018 </span>
													</time>
												</p>

											</div>
											<div class="home-blog-content blog-detail"
												style="background: #f7f7f7;">

												<p class="author" style="color: #ffffff">

													<i class="lnr lnr-user"></i> <span> Ram M</span>

												</p>

												<h4>
													<a href="blogs/news/blog-3.html" style="color: #434343;">Our
														Tech support is quick!</a>
												</h4>

												<p style="color: #999999;">Lorem ipsum dolor sit amet,
													consectetur adipiscing elit, sed do eiusmod tempor
													incididunt ut labore et dolore magna aliqua. Ut enim ad
													minim veniam, quis nostrud exerc...</p>

												<div class="blog-btn">
													<a class="btn" style="color: #ffffff;"
														href="blogs/news/blog-3.html">Read more<span
														class="fas fa-long-arrow-alt-right"></span></a>
												</div>

											</div>
										</div>

									</div>

								</div>

								<div class="article-item grid__item wow fadeInUp">
									<div class="article">
										<div class="home-blog-image grid__item">

											<a href="blogs/news/blog-2.html"> <img
												src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/articles/blog334a3.jpg?v=1518069122"
												alt="Plumbing work in the sink" />
											</a>

										</div>
										<div class="blog-description grid__item">
											<div class="blogs-sub-title">

												<p class="blog-date" style="color: #434343;">
													<time datetime="2018-01-11">
														<span class="date"><b style="color: #000000">11 </b> <br>
														<i> Jan</i><br> 2018 </span>
													</time>
												</p>

											</div>
											<div class="home-blog-content blog-detail"
												style="background: #f7f7f7;">

												<p class="author" style="color: #ffffff">

													<i class="lnr lnr-user"></i> <span> Ram M</span>

												</p>

												<h4>
													<a href="blogs/news/blog-2.html" style="color: #434343;">Plumbing
														work in the sink</a>
												</h4>

												<p style="color: #999999;">Lorem ipsum dolor sit amet,
													consectetur adipiscing elit, sed do eiusmod tempor
													incididunt ut labore et dolore magna aliqua. Ut enim ad
													minim veniam, quis nostrud exerc...</p>

												<div class="blog-btn">
													<a class="btn" style="color: #ffffff;"
														href="blogs/news/blog-2.html">Read more<span
														class="fas fa-long-arrow-alt-right"></span></a>
												</div>

											</div>
										</div>

									</div>

								</div>

								<div class="article-item grid__item wow fadeInUp">
									<div class="article">
										<div class="home-blog-image grid__item">

											<a href="blogs/news/blog-1.html"> <img
												src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/articles/blog2b1dc.jpg?v=1518069101"
												alt="Portable drilling machine" />
											</a>

										</div>
										<div class="blog-description grid__item">
											<div class="blogs-sub-title">

												<p class="blog-date" style="color: #434343;">
													<time datetime="2018-01-10">
														<span class="date"><b style="color: #000000">10 </b> <br>
														<i> Jan</i><br> 2018 </span>
													</time>
												</p>

											</div>
											<div class="home-blog-content blog-detail"
												style="background: #f7f7f7;">

												<p class="author" style="color: #ffffff">

													<i class="lnr lnr-user"></i> <span> Ram M</span>

												</p>

												<h4>
													<a href="blogs/news/blog-1.html" style="color: #434343;">Portable
														drilling machine</a>
												</h4>

												<p style="color: #999999;">Lorem ipsum dolor sit amet,
													consectetur adipiscing elit, sed do eiusmod tempor
													incididunt ut labore et dolore magna aliqua. Ut enim ad
													minim veniam, quis nostrud exerc...</p>

												<div class="blog-btn">
													<a class="btn" style="color: #ffffff;"
														href="blogs/news/blog-1.html">Read more<span
														class="fas fa-long-arrow-alt-right"></span></a>
												</div>

											</div>
										</div>

									</div>

								</div>

							</div>
							<div class="nav_article"></div>
						</div>

					</div>
				</div>

				<div class="dt-sc-hr-invisible-large"></div>

			</div>


		</div>
		<div id="shopify-section-1519189127347"
			class="shopify-section index-section">
			<div data-section-id="1519189127347"
				data-section-type="client section" class="client-section">

				<div class="dt-sc-hr-invisible-large"></div>
				<div class="grid-uniform section-seven">
					<div class="container">
						<div class="grid__item">

							<div class="border-title wow flash">

								<h2>Our Clients</h2>
							</div>

							<div id="client-car"
								class="owl-carousel owl-theme client-section">

								<div class="item  wow fadeInUp">
									<a href="#"> <img
										src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/7_250x250b8af.png?v=1519900351"
										alt="" /></a>
								</div>

								<div class="item  wow fadeInUp">
									<a href="#"> <img
										src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/8_250x2503d29.png?v=1519900353"
										alt="" /></a>
								</div>

								<div class="item  wow fadeInUp">
									<a href="#"> <img
										src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/9_250x2507ba4.png?v=1519900358"
										alt="" /></a>
								</div>

								<div class="item  wow fadeInUp">
									<a href="#"> <img
										src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/10_250x250d2f0.png?v=1519900360"
										alt="" /></a>
								</div>

								<div class="item  wow fadeInUp">
									<a href="#"> <img
										src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/11_250x250b078.png?v=1519900370"
										alt="" /></a>
								</div>

								<div class="item  wow fadeInUp">
									<a href="#"> <img
										src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/12_250x2502e45.png?v=1519900381"
										alt="" /></a>
								</div>

							</div>
						</div>
					</div>
				</div>
				<div class="dt-sc-hr-invisible-large"></div>

			</div>

		</div>
		<div id="shopify-section-1515670445231"
			class="shopify-section index-section">
			<div data-section-id="1515670445231"
				data-section-type="newsletter-block">
				<div class="newsletter-section" style=""ackground-color:#fab915;float:left;width:100%; ">
					<div class="overlay "></div>
					<div class="container ">

						<div class="border-title ">
							<h2 class="newslet_title " style="color: #434343;">Newsletter</h2>

							<div class="short-desc ">
								<p style="color: #ffffff;">Lorem ipsum dolor sit amet,
									consectetuer elit.</p>
							</div>

						</div>

						<form
							action="http://myshopify.us2.list-manage.com/subscribe/post?u=057ce5c6d3419dc4a568a2790&amp;id=78371397b0 "
							method="post " name="mc-embedded-subscribe-form "
							target="_blank " class="input-group mc-embedded-subscribe-form ">
							<input type="email " value=" " placeholder="Email address "
								name="EMAIL " class="mail " aria-label="Email address ">
							<button type="submit " class="btn subscribe " name="subscribe "
								value=" ">Subscribe</button>
						</form>

					</div>
				</div>
			</div>

		</div>
		<!-- END content_for_index -->

	</div>
