<!doctype html>
<html class="no-js">
<head>

<!-- Basic page needs ================================================== -->
<meta charset="utf-8">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

<link rel="shortcut icon"
	href="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/faviconcfcd.png?0"
	type="image/png" />

<!-- Title and description ================================================== -->
<title><?php echo $title?> - HSDtraders</title>

<!-- CSS ================================================== -->
    <?php $this->load->view('template/topcss');?>

    

</head>

<body id="nora" class="template-index">

	<div id="PageContainer"></div>
	<div class="quick-view"></div>
	<div id="shopify-section-header" class="shopify-section">
		<div data-section-id="header" data-section-type="header-type-3"
			class="header-type-3">

		<!-- header top layer -->
			<?php $this->load->view("template/header_top");?>
        <!-- end of  header top layer -->
			
		<!-- Main header -->
			<?php $this->load->view("template/header");?>
		<!-- end of Main header -->
			

    	<main class="main-content">
    		<?php $this->load->view($content);?>
    	</main>

		<?php $this->load->view("template/footer");?>



	<!-- Begin quick-view-template -->
	<div class="clearfix " id="quickview-template " style="display: none">
		<div class="overlay "></div>
		<div class="content clearfix ">
			<div class="product-img images ">
				<div class="quickview-featured-image product-photo-container "></div>
				<div class="more-view-wrapper ">
					<ul class="product-photo-thumbs quickview-more-views-owlslider ">
					</ul>
				</div>
			</div>
			<div class="product-shop summary ">
				<div class="product-item product-detail-section ">
					<h2 class="product-title ">
						<a>&nbsp;</a>
					</h2>
					<p class="item-text product-description "></p>
					<div class="prices product_price ">
						<label>Effective Price:</label> <span class="price h2 "
							id="QProductPrice "></span> <span class="compare-price "
							id="QComparePrice "></span>
					</div>

					<div class="product-infor ">
						<p class="product-inventory ">
							<label>Availability:</label><span></span>
						</p>
					</div>

					<div class="details clearfix ">
						<form action="https://nora-demo.myshopify.com/cart/add "
							method="post " class="variants ">
							<select name='id' style="display: none"></select>
							<div class="qty-section quantity-box ">
								<label>Quantity:</label>
								<div class="dec button qtyminus ">-</div>
								<input type="number " name="quantity " id="Qty " value="1 "
									class="quantity ">
								<div class="inc button qtyplus ">+</div>
							</div>

							<div class="total-price ">
								<label>Subtotal</label><span></span>
							</div>

							<div class="actions ">
								<button type="button " class="add-to-cart-btn btn ">Add to Cart
								</button>
							</div>

						</form>
					</div>
				</div>
			</div>
			<a href="javascript:void(0) " class="close-window "></a>
		</div>

	</div>
	<!-- End of quick-view-template -->


	<div class="loading-modal compare_modal">Loading</div>
	<div class="ajax-success-compare-modal compare_modal" id="moda-compare"
		tabindex="-1" role="dialog" style="display: none">
		<div class="overlay"></div>
		<div class="modal-dialog modal-lg">
			<div class="modal-content content" id="compare-modal">
				<div class="modal-header">

					<h4 class="modal-title">Compare Products</h4>
				</div>
				<div class="modal-body">
					<div class="table-wrapper">
						<table class="table table-hover">
							<thead>
								<tr class="th-compare">
									<th></th>
								</tr>
							</thead>
							<tbody id="table-compare">

							</tbody>

						</table>
					</div>
				</div>
				<div class="modal-footer">
					<a href="javascript:void(0)" class="close-modal"><i
						class="zmdi zmdi-close-circle"></i></a>
				</div>
			</div>
		</div>
	</div>

	<div class="loading-modal modal">Loading</div>
	<div class="ajax-error-modal modal">
		<div class="modal-inner">
			<div class="ajax-error-title">Error</div>
			<div class="ajax-error-message"></div>
		</div>
	</div>
	<div class="ajax-success-modal modal">
		<div class="overlay"></div>
		<div class="content">

			<div class="ajax-left">
				<p class="added-to-cart info">Product successfully added to your
					shopping cart</p>
				<p class="added-to-wishlist info">translation missing:
					en.products.wishlist.added_to_wishlist</p>
				<img class="ajax-product-image" alt="modal window" src="index.html" />
				<div class="ajax-cart-desc">
					<h3 class="ajax-product-title"></h3>
					<span class="ajax_price"></span>
					<p>
						Qty:&nbsp;<span class="ajax_qty"></span>
					</p>
				</div>
			</div>
			<div class="ajax-right">

				<p>
					There are <span class="ajax_cartCount"></span>&nbsp;items in your
					cart
				</p>
				<span class="ajax_cartTotal"></span>
				<button class="btn continue-shopping" onclick="javascript:void(0)">Continue
					shopping</button>
				<div class="success-message added-to-cart">
					<a href="cart.html" class="btn"><i class="fa fa-shopping-cart"></i>View
						Cart</a>
				</div>
				<!--  <div class="success-message added-to-wishlist"> <a href="/pages/wishlist" class="btn"><i class="fa fa-heart"></i>View Wishlist</a></div>                -->
			</div>
			<a href="javascript:void(0)" class="close-modal"><i
				class="fa fa-times-circle"></i></a>
		</div>
	</div>


	<div class="newsletterwrapper">
		<div id="email-modal" style="display: none;">
			<div class="modal-overlay"></div>
			<div class="modal-window" style="display: none;">
				<div class="window-window">
					<a class="btn close" title="Close Window"></a>

					<div class="window-content">

						<div class="grid__item">
							<img
								src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/popup_newsletter_imgcfcd.jpg?0"
								alt="Get to know the latest offers">
						</div>
						<div class="grid__item">
							<div id="mailchimp-email-subscibe">
								<div class="newsletter-title">
									<h2 class="title">Get to know the latest offers</h2>
								</div>
								<p class="message">Subscribe and get notified at first on the
									latest update and offers!</p>

								<div id="mc_embed_signup">

									<form
										action="http://myshopify.us2.list-manage.com/subscribe/post?u=057ce5c6d3419dc4a568a2790&amp;id=78371397b0"
										method="post" id="mc-embedded-subscribe-form"
										name="mc-embedded-subscribe-form" target="_blank">
										<input type="email" value="" placeholder="Email address"
											name="EMAIL" id="mail" aria-label="Email address">
										<button type="submit" class="btn" name="subscribe"
											id="subscribe">Send</button>
									</form>

								</div>
								<span>Note:we do not spam</span>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

                        
<?php $this->load->view('template/scripts');?>
</body>

<!-- Mirrored from nora-demo.myshopify.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 20 Aug 2019 06:22:34 GMT -->

</html>