<script src="<?php echo base_url();?>assets/code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/jquery-2.2.0.mincfcd.js?0" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>

<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/jquery-cookie.mincfcd.js?0" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/modernizr.mincfcd.js?0" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/jquery.flexslider.mincfcd.js?0" type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/jquery.stickycfcd.js?0" type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/bootstrap.mincfcd.js?0" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/jquery.fitvidcfcd.js?0" type="text/javascript"></script>
<script type="text/javascript">
    $('.dropdown.mega-menu').hover(function() {
        $('.main-content').addClass('call-overlay');
    }, function() {
        $('.main-content').removeClass('call-overlay');
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        var productSidedeals = $(".widget-featured-product .products-grid");
        productSidedeals.owlCarousel({
            loop: false,
            margin: 10,
            nav: true,
            navContainer: ".widget-featured-nav",
            navText: [' <a class="prev active"><i class="icon-arrow-left icons"></i></a>', ' <a class="prev active"><i class="icon-arrow-right icons"></i></a>'],
            dots: false,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                1000: {
                    items: 1
                }
            }
        });
    });
</script>
<script type="text/javascript">
    $('.dropdown.mega-menu').hover(function() {
        $('.main-content').addClass('call-overlay');
    }, function() {
        $('.main-content').removeClass('call-overlay');
    });
</script>
<script>
    var prodVideoId = $('iframe').data('video-id');
    $(document).on('ready', function() {
        $('.variable-width').fitVids().slick({
            dots: true,
            slidesToScroll: 1,
            autoplay: true,
            fade: true,
            autoplaySpeed: 3000,
            afterChange: function(slick, currentSlide) {
                console.log(currentSlide);
            }
        });
    })
</script>
<script type="text/javascript">
    $(document).ready(function() {
        var type9_products = $('.type9__items.1515665844410');
        type9_products.owlCarousel({
            loop: true,
            margin: 0,
            nav: true,
            navContainer: ".nav-type9__items.1515665844410",
            navText: ['<a class="prev active btn"><i class="icon-arrow-left icons"></i></a>', '<a class="next btn"><i class="icon-arrow-right icons"></i></a>'],
            dots: false,
            //   autoplay: true,
            smartSpeed: 1000,
            navSpeed: 1000,
            //  autoplayTimeout: 3000,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                768: {
                    items: 1
                }
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        var type9_products = $('.type9__items.1515670483401');
        type9_products.owlCarousel({
            loop: true,
            margin: 0,
            nav: true,
            navContainer: ".nav-type9__items.1515670483401",
            navText: ['<a class="prev active btn"><i class="icon-arrow-left icons"></i></a>', '<a class="next btn"><i class="icon-arrow-right icons"></i></a>'],
            dots: false,
            //   autoplay: true,
            smartSpeed: 1000,
            navSpeed: 1000,
            //  autoplayTimeout: 3000,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 2
                },
                768: {
                    items: 1
                }
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        BlogType4 = $("#BlogType4");
        BlogType4.owlCarousel({
            loop: false,
            margin: 15,
            autoPlay: false,
            nav: true,
            navContainer: ".blog-post-type-4 .nav_article",
            navText: ['<a class="prev btn"><i class="icon-arrow-left icons" ></i></a>', '<a class="next btn"><i class="icon-arrow-right icons" ></i></a>'],
            dots: false,
            responsive: {
                0: {
                    items: 1
                },
                600: {
                    items: 1
                },
                720: {
                    items: 2
                }
            }
        });
    });
</script>
<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/timbercfcd.js?0 " type="text/javascript "></script>
<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/jquery.inviewcfcd.js?0 " type="text/javascript "></script>
<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/jquery-easing-1.3cfcd.js?0 " type="text/javascript "></script>
<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/themecfcd.js?0 " type="text/javascript "></script>
<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/assets/themes_support/option_selection-fe6b72c2bbdd3369ac0bfefe8648e3c889efca213baefd4cfb0dd9363563831f.js " type="text/javascript "></script>
<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/assets/themes_support/api.jquery-e94e010e92e659b566dbc436fdfe5242764380e00398907a14955ba301a4749f.js " type="text/javascript "></script>
<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/jquery.historycfcd.js?0 " type="text/javascript "></script>
<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/jquery.storageapi.mincfcd.js?0 " type="text/javascript "></script>
<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/smartalertcfcd.js?0 " type="text/javascript "></script>
<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/lightslider.mincfcd.js?0 " type="text/javascript "></script>
<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/shopcfcd.js?0 " type="text/javascript "></script>
<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/owl.carousel.mincfcd.js?0 " type="text/javascript "></script>
<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/jquery-ui-totopcfcd.js?0 " type="text/javascript "></script>
<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/jquery.tabs.mincfcd.js?0 " type="text/javascript "></script>
<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/slickcfcd.js?0 " type="text/javascript "></script>
<script type="text/javascript">
    // <![CDATA[
    jQuery(document).ready(function() { //
        var $modalParent = jQuery('div.newsletterwrapper'),
            modalWindow = jQuery('#email-modal'),
            emailModal = jQuery('#email-modal'),
            modalPageURL = window.location.pathname;

        modalWindow = modalWindow.html();
        modalWindow = '<div id="email-modal">' + modalWindow + '</div>';
        $modalParent.css({
            'position': 'relative'
        });
        jQuery('.wrapper #email-modal').remove();
        $modalParent.append(modalWindow);

        if (jQuery.cookie('emailSubcribeModal') != 'closed') {
            openEmailModalWindow();
        };

        jQuery('#email-modal .btn.close').click(function(e) {
            e.preventDefault();
            closeEmailModalWindow();
        });
        jQuery('body').keydown(function(e) {
            if (e.which == 27) {
                closeEmailModalWindow();
                jQuery('body').unbind('keydown');
            }
        });
        jQuery('#mc_embed_signup form').submit(function() {
            if (jQuery('#mc_embed_signup .email').val() != '') {
                closeEmailModalWindow();
            }
        });

        function closeEmailModalWindow() {
            jQuery('#email-modal .modal-window').fadeOut(600, function() {
                jQuery('#email-modal .modal-overlay').fadeOut(600, function() {
                    jQuery('#email-modal').hide();
                    jQuery.cookie('emailSubcribeModal', 'closed', {
                        expires: 1,
                        path: '/'
                    });
                });
            })
        }

        function openEmailModalWindow() {
            jQuery('#email-modal').fadeIn(600, function() {
                jQuery('#email-modal .modal-window').fadeIn(600);
            });
        }

    });
    // ]]
    // ]]>
</script>
<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/wowcfcd.js?0" type="text/javascript"></script>

<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/classiecfcd.js?0"></script>
<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/latest-productscfcd.js?0"></script>
<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/jquery.prettyPhotocfcd.js?0"></script>

<script>
    $('.qtyplus').click(function(e) {
        e.preventDefault();
        var currentVal = parseInt($('input[name="quantity"]').val());
        if (!isNaN(currentVal)) {
            $('input[name="quantity"]').val(currentVal + 1);
        } else {
            $('input[name="quantity"]').val(1);
        }

    });

    $(".qtyminus").click(function(e) {

        e.preventDefault();
        var currentVal = parseInt($('input[name="quantity"]').val());
        if (!isNaN(currentVal) && currentVal > 0) {
            $('input[name="quantity"]').val(currentVal - 1);
        } else {
            $('input[name="quantity"]').val(1);
        }

    });
</script>

<script type="text/javascript">
    $('.quick-view .close-window').click(function() {
        $('.quick-view').switchClass("open-in", "open-out");
    });
</script>
<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/uisearchcfcd.js?0"></script>

<!-- End Recently Viewed Products -->

<script src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/magnific-popupcfcd.js?0" type="text/javascript"></script>
<script type="text/javascript">
    if ($('.p-video').length > 0) {
        $('.jas-popup-url').magnificPopup({
            disableOn: 0,
            tLoading: '<div class="loader"><div class="loader-inner"></div></div>',
            type: 'iframe'
        });
    }
</script>