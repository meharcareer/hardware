<div id="shopify-section-footer-model-6 " class="shopify-section ">
		<div data-section-id="footer-model-6 "
			data-section-type="Footer-model-6 " class="footer-model-6 ">
			<footer class="site-footer " style="background: #f7f7f7;">
				<div class="grid-uniform ">
					<div class="footer-newsletter-section "
						style="border-bottom: 1px solid rgba(0, 0, 0, 0);">
						<div class="container ">
							<div class="grid__item " style="position: relative;">
								<div class="footer-logo ">

									<a href="index.html "> <img class="normal-footer-logo "
										src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/2018-04-02_400x6f7c.png?v=1522674601 "
										alt="Nora " />
									</a>

								</div>

								<div class="footer_social_links ">
									<ul class="inline-list social-icons social-links-type-1 ">

										<li><a style="color: #434343;"
											class="icon-fallback-text twitt hexagon " target="blank "
											href="https://twitter.com/shopify "
											onmouseover="this.style.color='#fab915' ; "
											onmouseout="this.style.color='#434343' ; " title="Twitter ">
												<span class="fab fa-twitter " aria-hidden="true "></span>
										</a></li>

										<li><a style="color: #434343;"
											class="icon-fallback-text fb hexagon " target="blank "
											href="https://www.facebook.com/shopify "
											onmouseover="this.style.color='#fab915' ; "
											onmouseout="this.style.color='#434343' ; " title="Facebook ">
												<span class="fab fa-facebook " aria-hidden="true "></span> <span
												class="fallback-text ">Facebook</span>
										</a></li>

										<li><a style="color: #434343;"
											class="icon-fallback-text google hexagon " target="blank "
											href="# " onmouseover="this.style.color='#fab915' ; "
											onmouseout="this.style.color='#434343' ; " title="Google+ "
											rel="publisher "> <span class="fab fa-google "
												aria-hidden="true "></span> <span class="fallback-text ">Google</span>
										</a></li>

										<li><a style="color: #434343;"
											class="icon-fallback-text tumblr " target="blank " href="# "
											onmouseover="this.style.color='#fab915' ; "
											onmouseout="this.style.color='#434343' ; " title="Tumblr "> <span
												class="fab fa-tumblr " aria-hidden="true "></span> <span
												class="fallback-text ">Tumblr</span>
										</a></li>

									</ul>

								</div>

							</div>
						</div>
					</div>

					<div class="grid-uniform ">
						<div class="container ">

							<div
								class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half ">

								<h4 style="color: #434343;">Contact us</h4>

								<div class="address ">

									<p style="color: #434343;">1203 Town Center Drive FL 33458 USA</p>

									<p style="color: #434343;">
										<i class="fas fa-phone-volume "></i>+841 123 456 78
									</p>

								</div>

								<i class="far fa-envelope "></i><a style="color: #434343;"
									href="mailto:info@lindashop.com ">info@lindashop.com</a>

							</div>

							

							<div
								class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half ">

								<h4 style="color: #434343;">Categories</h4>

								<ul class="site-footer__links ">

									<li><a style="color: #434343;" href="<?php echo base_url("product/all_products");?>">
											Category 1</a></li>

									<li><a style="color: #434343;" href="<?php echo base_url("product/all_products");?>">
											Category 2</a></li>

									<li><a style="color: #434343;" href="<?php echo base_url("product/all_products");?>">
											Category 3</a></li>

									<li><a style="color: #434343;" href="<?php echo base_url("product/all_products");?>">
											Category 4</a></li>

									<li><a style="color: #434343;" href="<?php echo base_url("product/all_products");?>">
											Category 5 </a></li>

								</ul>
							</div>

							<div
								class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half ">

								<h4 style="color: #434343;">Information</h4>

								<ul class="site-footer__links ">

									<li><a style="color: #434343;" href="<?php echo base_url("home/contact");?>">Contact
											Us</a></li>

									<li><a style="color: #434343;" href="<?php echo base_url();?>home/about">About
											Us</a></li>

									<li><a style="color: #434343;" href="<?php echo base_url("home/gallery")?>">Gallery</a></li>

								</ul>
							</div>
							<div
								class="grid__item wide--one-quarter post-large--one-quarter large--one-quarter medium--one-half ">

								<h4 style="color: #434343;">My Account</h4>

								<ul class="site-footer__links ">
									<li><a style="color: #434343;" href="<?php echo base_url("home/contact")?>">Store Locations</a></li>
									<li><a style="color: #434343;" href="<?php echo base_url("home/terms")?>">Orders and Returns</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

				<div class="footer-bottom ">

					<div class="grid__item footer-bottom__menu "
						style="background: rgba(0, 0, 0, 0);">
						<ul>

							<li><a style="color: #434343; border-right: 1px solid #434343;"
								href="<?php echo base_url("home/contact")?>">Store Locations</a></li>

							<li><a style="color: #434343; border-right: 1px solid #434343;"
								href="<?php echo base_url("home/terms")?>">Orders and Returns</a></li>

						</ul>
					</div>

				</div>
			</footer>

			<style>
</style>

			<div class="grid__item ">
				<div class="copyright " style="background: #efefef">
					<div class="container ">

						<p class="copyright_left " style="color: #434343">
							Copyright &copy; 2019, Hsdtraders <a target="_blank " rel="nofollow "
								href="<?php echo base_url();?>">Developed
								by Mehar</a>

						</p>

						<div class="payment_section right ">
							<div class="footer-icons ">
								<ul class="inline-list payment-icons ">
									<li><a href="#"><img
											src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/paypal-48_small6e16.png?v=1520313389 "
											alt="payment_icon_1 " /></a></li>
									<li><a href="#"><img
											src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/cirrus-48_small9f52.png?v=1520313399 "
											alt="payment_icon_2 " /></a></li>
									<li><a href="#"><img
											src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/american-express-48_smallb186.png?v=1520313408 "
											alt="payment_icon_3 " /></a></li>
									<li><a href="#"><img
											src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/visa-electron_small9204.png?v=1520313429 "
											alt="payment_icon_4 " /></a></li>
									<li><a href="#"><img
											src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/files/maestro-48_small2e52.png?v=1520313451 "
											alt="payment_icon_5 " /></a></li>

								</ul>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

	</div>
