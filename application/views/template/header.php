
<header class="site-header">
				<div class="header-sticky">
					<div id="header-landing" class="sticky-animate">
						<div class="grid--full site-header__menubar">
							<div class="container">
								<div class="menubar_container"
									style="position: relative; float: left; width: 100%;">

									<h1
										class="grid__item wide--one-sixth post-large--one-sixth large--one-sixth site-header__logo"
										itemscope itemtype="http://schema.org/Organization">

										<a href="<?php echo base_url();?>home" style="max-width: 450px;"> <img
											class="normal-logo"
											src="<?php echo base_url();?>assets/cdn.shopify.com/s/files/1/2721/6956/t/3/assets/logocfcd.png?0"
											alt="Nora" itemprop="logo">
										</a>

									</h1>

									<div
										class="grid__item wide--five-sixths post-large--five-sixths large--five-sixths menubar-section">
										<div class="menu-tool medium-down--hide">
											<div class="menu-tool">
												<div class="container">

													<ul class="site-nav">

														<li><a class="<?php echo ($content == "home") ? "current":"";?>" href="<?php echo base_url();?>home"> Home </a>

															<ul class="site-nav-dropdown">

															</ul></li>
															
														<li><a class="<?php echo ($content == "about") ? "current":"";?>" href="<?php echo base_url();?>home/about"> About Us </a>

															<ul class="site-nav-dropdown">

															</ul></li>

														<li class="dropdown mega-menu"><a class=""
															href="<?php echo base_url("product/all_products");?>"> Equipments <i
																class="zmdi zmdi-caret-down"></i>
														</a>

															<div class="site-nav-dropdown">

																<div class="container style_3">
																	<div class="col-1 parent-mega-menu">

																		<div class="inner">
																			<!-- Menu level 2 -->
																			<a href="<?php echo base_url("product/all_products");?>" class=""> Tools </a>

																			<ul class="dropdown">

																				<!-- Menu level 3 -->
																				<li><a href="<?php echo base_url("product/all_products");?>" class=" "> Wrench
																				</a></li>

																				<!-- Menu level 3 -->
																				<li><a href="<?php echo base_url("product/all_products");?>" class=" ">
																						Spanner </a></li>

																				<!-- Menu level 3 -->
																				<li><a href="<?php echo base_url("product/all_products");?>" class=" ">
																						Drilling machine </a></li>

																				<!-- Menu level 3 -->
																				<li><a href="<?php echo base_url("product/all_products");?>" class=" "> PVC
																						Pipe Cutter </a></li>

																			</ul>

																		</div>

																		<div class="inner">
																			<!-- Menu level 2 -->
																			<a href="<?php echo base_url("product/all_products");?>" class=""> Pipes </a>

																			<ul class="dropdown">

																				<!-- Menu level 3 -->
																				<li><a href="<?php echo base_url("product/all_products");?>" class=" "> Union
																						with Locknut </a></li>

																				<!-- Menu level 3 -->
																				<li><a href="<?php echo base_url("product/all_products");?>" class=" "> Hub
																						Coupling fits </a></li>

																				<!-- Menu level 3 -->
																				<li><a href="<?php echo base_url("product/all_products");?>" class=" ">
																						waterproof seal </a></li>

																				<!-- Menu level 3 -->
																				<li><a href="<?php echo base_url("product/all_products");?>" class=" "> Brass
																						Push-Fit </a></li>

																			</ul>

																		</div>

																		<div class="inner">
																			<!-- Menu level 2 -->
																			<a href="<?php echo base_url("product/all_products");?>" class=""> pipe seal </a>

																			<ul class="dropdown">

																				<!-- Menu level 3 -->
																				<li><a href="<?php echo base_url("product/all_products");?>" class=" ">
																						Rubberized Waterproof Tape </a></li>

																				<!-- Menu level 3 -->
																				<li><a href="<?php echo base_url("product/all_products");?>" class=" "> Non
																						Flammable </a></li>

																				<!-- Menu level 3 -->
																				<li><a href="<?php echo base_url("product/all_products");?>" class=" "> Thread
																						Seal </a></li>

																				<!-- Menu level 3 -->
																				<li><a href="<?php echo base_url("product/all_products");?>" class=" "> Less
																						Sticky on finger </a></li>

																			</ul>

																		</div>

																		<div class="inner">
																			<!-- Menu level 2 -->
																			<a href="<?php echo base_url("product/all_products");?>" class=""> Service </a>

																			<ul class="dropdown">

																				<!-- Menu level 3 -->
																				<li><a href="<?php echo base_url("product/all_products");?>" class=" ">
																						Trenchless pipe repair </a></li>

																				<!-- Menu level 3 -->
																				<li><a href="<?php echo base_url("product/all_products");?>" class=" "> Frozen
																						pipes </a></li>

																				<!-- Menu level 3 -->
																				<li><a href="<?php echo base_url("product/all_products");?>" class=" "> Camera
																						inspections </a></li>

																				<!-- Menu level 3 -->
																				<li><a href="<?php echo base_url("product/all_products");?>" class=" "> Common
																						Repair </a></li>

																			</ul>

																		</div>

																	</div>

																</div>

															</div></li>

														<li class="dropdown "><a class="" href="<?php echo base_url("home/gallery")?>">
																Gallery <!-- <i class="zmdi zmdi-caret-down"></i> -->
														</a>

<!-- 															<ul class="site-nav-dropdown"> 

																<li><a href="<?php //echo base_url("home/gallery/window")?>" class=""> <span> Window Hardwares </span>

<!-- 																</a> -->
<!-- 																	<ul class="site-nav-dropdown"> -->

<!-- 																	</ul></li>

																<li><a href="<?php //echo base_url("home/gallery/door")?>" class=""> <span> Door Hardwares </span>

<!-- 																</a> -->
<!-- 																	<ul class="site-nav-dropdown"> -->

<!-- 																	</ul></li> -->

<!-- 															</ul> --></li>

														<li><a class="" href="<?php echo base_url();?>home/contact"> Contact Us </a>

															<ul class="site-nav-dropdown">

															</ul></li>


													</ul>
												</div>
											</div>
										</div>
<!-- 										<div class="menu-icon"> -->

<!-- 											<div class="header-search"> -->
<!-- 												<a href="search.html" -->
<!-- 													class="site-header__link site-header__search js-drawer-open-top"> -->
<!-- 													<span class="fa fa-search" aria-hidden="true"></span> -->
<!-- 												</a> -->
<!-- 											</div> -->

<!-- 											<div class="header-bar__module cart header_cart"> -->
												<!-- Mini Cart Start -->
<!-- 												<div class="baskettop"> -->
<!-- 													<div class="wrapper-top-cart"> -->
<!-- 														<a href="javascript:void(0)" id="ToggleDown" -->
<!-- 															class="icon-cart-arrow"> <i class="fa fa-shopping-cart" -->
<!-- 															aria-hidden="true"></i> -->
<!-- 															<div id="cartCount">0</div> -->

<!-- 														</a> -->
														<div id="slidedown-cart" style="display: none">
															<!--  <h3>Shopping cart</h3>-->
<!-- 															<div class="no-items"> -->
<!-- 																<p>Your cart is currently empty!</p> -->
<!-- 																<p class="text-continue"> -->
<!-- 																	<a class="btn" href="javascript:void(0)">Continue -->
<!-- 																		shopping</a> -->
<!-- 																</p> -->
<!-- 															</div> -->
<!-- 															<div class="has-items"> -->
<!-- 																<ul class="mini-products-list"> -->

<!-- 																</ul> -->
<!-- 																<div class="summary"> -->
<!-- 																	<p class="total"> -->
<!-- 																		<span class="label">Cart total:</span> <span -->
<!-- 																			class="price"><span class=money>$0.00</span></span> -->
<!-- 																	</p> -->
<!-- 																</div> -->
<!-- 																<div class="actions"> -->
<!-- 																	<button class="btn" -->
																		onclick="window.location='cart.html'">
<!-- 																		<i class="icon-check"></i>Check Out -->
<!-- 																	</button> -->
<!-- 																	<button class="btn text-cart" -->
																		onclick="window.location='cart.html'">
<!-- 																		<i class="icon-basket"></i>View Cart -->
<!-- 																	</button> -->
<!-- 																</div> -->
<!-- 															</div> -->
<!-- 														</div> -->
<!-- 													</div> -->
<!-- 												</div> -->
												<!-- End Top Header -->
<!-- 											</div> -->

<!-- 											<ul class="menu_bar_right"> -->
<!-- 												<li> -->
<!-- 													<div class="slidedown_section"> -->
<!-- 														<a id="Togglemodal" class="icon-cart-arrow"><i -->
<!-- 															class="fa fa-bars"></i></a> -->
<!-- 														<div id="slidedown-modal"> -->
<!-- 															<div class="header-panel-top"> -->
<!-- 																<div class="customer_account"> -->
<!-- 																	<ul> -->

<!-- 																		<li><a href="account/login.html" title="Log in">Log in</a> -->
<!-- 																		</li> -->
<!-- 																		<li><a href="account/register.html" -->
<!-- 																			title="Create account">Create account</a></li> -->

<!-- 																		<li><a class="wishlist" href="pages/wishlist.html" -->
<!-- 																			title="Wishlist">Wishlist</a></li> -->

<!-- 																	</ul> -->
<!-- 																</div> -->

<!-- 															</div> -->
<!-- 														</div> -->
<!-- 													</div> -->
<!-- 												</li> -->
<!-- 											</ul> -->

<!-- 										</div> -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
			
			<div
				class="mobile-nav-section wide--hide post-large--hide large--hide">
				<button type="button" class="mobile-nav-trigger"
					id="MobileNavTrigger">
					<span class="icon-menu" aria-hidden="true"></span>
				</button>
			</div>
			<ul id="MobileNav"
				class="mobile-nav wide--hide post-large--hide large--hide">

				<li class="mobile-nav__link" aria-haspopup="true"><a
					href="index.html" class="mobile-nav current"> Home </a></li>

				<li class="mobile-nav__link" aria-haspopup="true"><a
					href="collections/construction.html" class=""> Equipments </a> <span
					class="mobile-nav__sublist-trigger"><span
						class="mobile-nav__sublist-expand"> <span class="fallback-text">+</span>
					</span> <span class="mobile-nav__sublist-contract"> <span
							class="fallback-text">-</span>
					</span> </span>
					<ul class="mobile-nav__sublist">

						<li class="mobile-nav__link" aria-haspopup="true"><a
							href="<?php echo base_url("product/all_products");?>" class=""> Tools </a> <span
							class="mobile-nav__sublist-trigger"><span
								class="mobile-nav__sublist-expand"> <span class="fallback-text">+</span>
							</span> <span class="mobile-nav__sublist-contract"> <span
									class="fallback-text">-</span>
							</span> </span>

							<ul class="mobile-nav__sublist">

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Wrench</a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Spanner </a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Drilling machine</a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">PVC Pipe Cutter</a></li>

							</ul></li>

						<li class="mobile-nav__link" aria-haspopup="true"><a
							href="<?php echo base_url("product/all_products");?>" class=""> Pipes </a> <span
							class="mobile-nav__sublist-trigger"><span
								class="mobile-nav__sublist-expand"> <span class="fallback-text">+</span>
							</span> <span class="mobile-nav__sublist-contract"> <span
									class="fallback-text">-</span>
							</span> </span>

							<ul class="mobile-nav__sublist">

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Union with Locknut</a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Hub Coupling fits</a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">waterproof seal</a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Brass Push-Fit</a></li>

							</ul></li>

						<li class="mobile-nav__link" aria-haspopup="true"><a
							href="<?php echo base_url("product/all_products");?>" class=""> pipe seal </a> <span
							class="mobile-nav__sublist-trigger"><span
								class="mobile-nav__sublist-expand"> <span class="fallback-text">+</span>
							</span> <span class="mobile-nav__sublist-contract"> <span
									class="fallback-text">-</span>
							</span> </span>

							<ul class="mobile-nav__sublist">

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Rubberized Waterproof Tape</a>
								</li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Non Flammable</a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Thread Seal</a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Less Sticky on finger</a>
								</li>

							</ul></li>

						<li class="mobile-nav__link" aria-haspopup="true"><a
							href="<?php echo base_url("product/all_products");?>" class=""> Service </a> <span
							class="mobile-nav__sublist-trigger"><span
								class="mobile-nav__sublist-expand"> <span class="fallback-text">+</span>
							</span> <span class="mobile-nav__sublist-contract"> <span
									class="fallback-text">-</span>
							</span> </span>

							<ul class="mobile-nav__sublist">

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Trenchless pipe repair </a>
								</li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Frozen pipes </a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Camera inspections </a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Common Repair</a></li>

							</ul></li>

					</ul></li>

				<li class="mobile-nav__link" aria-haspopup="true"><a
					href="<?php echo base_url("product/all_products");?>" class=""> Shop </a> <span
					class="mobile-nav__sublist-trigger"><span
						class="mobile-nav__sublist-expand"> <span class="fallback-text">+</span>
					</span> <span class="mobile-nav__sublist-contract"> <span
							class="fallback-text">-</span>
					</span> </span>
					<ul class="mobile-nav__sublist">

						<li class="mobile-nav__link" aria-haspopup="true"><a
							href="<?php echo base_url("product/all_products");?>" class=""> Spanner </a> <span
							class="mobile-nav__sublist-trigger"><span
								class="mobile-nav__sublist-expand"> <span class="fallback-text">+</span>
							</span> <span class="mobile-nav__sublist-contract"> <span
									class="fallback-text">-</span>
							</span> </span>

							<ul class="mobile-nav__sublist">

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Steel Pipe Wrench</a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Multi Wrench Spanner</a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Household Spanner </a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Professional Pipe wrench </a>
								</li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Doe Spanner Set</a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Adjustable Spanner</a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class=""> Combination</a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Universal Wrenches</a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Double Ended </a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Hex Key Set </a></li>

							</ul></li>

						<li class="mobile-nav__link" aria-haspopup="true"><a
							href="<?php echo base_url("product/all_products");?>" class=""> Paint brushes </a> <span
							class="mobile-nav__sublist-trigger"><span
								class="mobile-nav__sublist-expand"> <span class="fallback-text">+</span>
							</span> <span class="mobile-nav__sublist-contract"> <span
									class="fallback-text">-</span>
							</span> </span>

							<ul class="mobile-nav__sublist">

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Wall Decoration brush</a>
								</li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Gam Paint brush</a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Sash paint brush</a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Brass Brush</a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Spray Painting Machine</a>
								</li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Wall Paint Brush</a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Brush Roller</a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Brush Multicolour </a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Paint Sprayer</a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Eco-Friendly Angle </a></li>

							</ul></li>

						<li class="mobile-nav__link" aria-haspopup="true"><a
							href="<?php echo base_url("product/all_products");?>" class=""> Tape </a> <span
							class="mobile-nav__sublist-trigger"><span
								class="mobile-nav__sublist-expand"> <span class="fallback-text">+</span>
							</span> <span class="mobile-nav__sublist-contract"> <span
									class="fallback-text">-</span>
							</span> </span>

							<ul class="mobile-nav__sublist">

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Emergency Repair tape</a>
								</li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Surface Protection Tape</a>
								</li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Thread Seal Tape</a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Inch Tape</a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Emergency Repair </a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Double Sided Self </a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Rubberized </a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">Ceramic Disk </a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class="">TeflonTape </a></li>

								<li class="mobile-nav__sublist-link"><a
									href="<?php echo base_url("product/all_products");?>" class=""> Insulation Foam </a></li>

							</ul></li>

					</ul></li>

				<li class="mobile-nav__link" aria-haspopup="true"><a
					href="blogs/news.html" class=""> Pages </a> <span
					class="mobile-nav__sublist-trigger"><span
						class="mobile-nav__sublist-expand"> <span class="fallback-text">+</span>
					</span> <span class="mobile-nav__sublist-contract"> <span
							class="fallback-text">-</span>
					</span> </span>
					<ul class="mobile-nav__sublist">

						<li class="mobile-nav__sublist-link"><a class="mobile-nav "
							href="pages/about-us.html">About Us</a></li>

						<li class="mobile-nav__sublist-link"><a class="mobile-nav "
							href="blogs/news.html">Blog</a></li>

						<li class="mobile-nav__sublist-link"><a class="mobile-nav "
							href="<?php echo base_url();?>home/contact">Contact Us</a></li>

						<li class="mobile-nav__sublist-link"><a class="mobile-nav "
							href="pages/faq.html">Faq</a></li>

					</ul></li>

				<li class="mobile-nav__link" aria-haspopup="true"><a
					href="collections.html" class="mobile-nav "> Hardware </a></li>

				<li class="mobile-nav__link" aria-haspopup="true"><a
					href="products/product-1.html" class="mobile-nav "> Tools </a></li>

			</ul>

		</div>

	</div>