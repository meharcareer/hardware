<div class="top_bar top-bar-type-3">
				<div class="container">

					<div class="header_social top_bar_left">

						<ul class="inline-list social-icons social-links-type-1">

							<li><a style="color:;" class="icon-fallback-text twitt hexagon"
								target="blank" href="https://twitter.com/shopify"
								onmouseover="this.style.color='';"
								onmouseout="this.style.color='';" title="Twitter"> <span
									class="fab fa-twitter" aria-hidden="true"></span>
							</a></li>

							<li><a style="color:;" class="icon-fallback-text fb hexagon"
								target="blank" href="https://www.facebook.com/shopify"
								onmouseover="this.style.color='';"
								onmouseout="this.style.color='';" title="Facebook"> <span
									class="fab fa-facebook" aria-hidden="true"></span> <span
									class="fallback-text">Facebook</span>
							</a></li>

							<li><a style="color:;" class="icon-fallback-text google hexagon"
								target="blank" href="#" onmouseover="this.style.color='';"
								onmouseout="this.style.color='';" title="Google+"
								rel="publisher"> <span class="fab fa-google" aria-hidden="true"></span>
									<span class="fallback-text">Google</span>
							</a></li>

							<li><a style="color:;" class="icon-fallback-text tumblr"
								target="blank" href="#" onmouseover="this.style.color='';"
								onmouseout="this.style.color='';" title="Tumblr"> <span
									class="fab fa-tumblr" aria-hidden="true"></span> <span
									class="fallback-text">Tumblr</span>
							</a></li>

						</ul>

					</div>

					<div class="top_bar_menu">
						<ul
							class="menu_bar_right grid__item wide--one-sixth post-large--one-sixth">

							<li>
								<ul class="top_bar_right">

									<li><span>Call Us : (00) 000 111 222</span></li>

									<li>Mail : <a href="mailto:info@somedomain.com">
											info@somedomain.com</a></li>

								</ul>
							</li>

						</ul>
					</div>
				</div>
			</div>
			
			
<!-- *********************************************************************************************************** -->
<div id="SearchDrawer"
				class="search-bar drawer drawer--top search-bar-type-3">
				<div class="search-bar__table">
					<form action="https://nora-demo.myshopify.com/search" method="get"
						class="search-bar__table-cell search-bar__form" role="search">
						<input type="hidden" name="type" value="product">
						<div class="search-bar__table">
							<div class="search-bar__table-cell search-bar__icon-cell">
								<button type="submit"
									class="search-bar__icon-button search-bar__submit">
									<span class="fa fa-search" aria-hidden="true"></span>
								</button>
							</div>
							<div class="search-bar__table-cell">
								<input type="search" id="SearchInput" name="q" value=""
									placeholder="Search..." aria-label="Search..."
									class="search-bar__input">
							</div>
						</div>
					</form>
					<div class="search-bar__table-cell text-right">
						<button type="button"
							class="search-bar__icon-button search-bar__close js-drawer-close">
							<span class="fa fa-times" aria-hidden="true"></span>
						</button>
					</div>
				</div>
			</div>